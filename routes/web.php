<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/','TestController@index');
Route::get('/get-name','HomeController@getName');

Route::get('/without-javascript','JavascriptController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('admin-login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin-login','Auth\AdminLoginController@logout')->name('logout-admin');
Route::post('admin','Auth\AdminLoginController@login')->name('admin.login.submit');

Route::group(['middleware'=>'superadmin'],function (){
    Route::resource('company','CompanyController');
    Route::resource('role','RoleController');
    Route::resource('metric','MetricController');
    Route::resource('food-category','FoodCategoryController');
    Route::resource('user','UserController');
    Route::resource('cuisine','CuisineController');
    Route::resource('partner','PartnerController');
    Route::resource('subscription-category','SubscriptCategoryController');
    Route::resource('subscription-package','SubscriptPackageController');
    Route::resource('subscription','SubscriptionController');
    Route::resource('general-product','GeneralProductController');
    Route::get('show-companies','ProductController@getCompanies');
    Route::resource('translation-group','TranslationGroupController');
});

Route::prefix('settings')->group(function () {
    Route::resource('deliver-time','DeliveryTimesController');
    Route::resource('deliver-detail','DeliveryDetailsController');
    Route::resource('edit-company','EditCompanyController');
    Route::resource('holiday','HolidayController');
    Route::resource('working-hours','WorkingHoursController');
    Route::resource('delivery-price-category','DeliveryPriceCategoryController');
});

Route::put('update-delivery/{id}','EditCompanyController@update_delivery');
Route::resource('recommendation','RecommendationController');
Route::resource('product','ProductController');
Route::resource('availability','AvailabilityController');
Route::get('place-order','OrderController@searchRestaurants');
Route::get('restaurant/zip/{zip}','OrderController@showRestaurant');
Route::resource('order','OrderController');
Route::resource('general-product-ingredient','IngredientsController');
Route::resource('product-image','ProductImageController');
Route::resource('create-menu','CreateMenuController');
Route::resource('language','LanguageController');
Route::resource('translation','TranslateController');



