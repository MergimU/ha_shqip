<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Models\Admin();
        $admin->company_id = 1;
        $admin->name = 'Mergim';
        $admin->surname = 'Uka';
        $admin->email = 'mergim@mergim.com';
        $admin->password = bcrypt('mergim');
        $admin->save();

        $admin = new \App\Models\Admin();
        $admin->company_id = 1;
        $admin->name = 'Agon';
        $admin->surname = 'Bajgora';
        $admin->email = 'agon@agon.com';
        $admin->password = bcrypt('mergim');
        $admin->save();


    }
}
