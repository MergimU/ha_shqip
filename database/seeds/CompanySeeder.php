<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = new \App\Company();
        $company->name = "Extreme-Grill";
        $company->save();

        $company = new \App\Company();
        $company->name = "Gjelltore Nazi";
        $company->save();
    }
}
