<?php

use Illuminate\Database\Seeder;

class AdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = new \App\AdminRole();
        $admin_role->admin_id = 1;
        $admin_role->role_id = 1;
        $admin_role->save();

        $admin_role->admin_id = 2;
        $admin_role->role_id = 2;
        $admin_role->save();
    }
}
