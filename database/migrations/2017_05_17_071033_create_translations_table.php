<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('translation');
            $table->integer('language_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('group_translation_id')->unsigned();
            $table->timestamps();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('group_translation_id')->references('id')->on('translation_groups');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translations');
    }
}
