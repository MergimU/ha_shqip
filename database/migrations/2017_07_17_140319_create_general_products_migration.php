<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralProductsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('food_category_id')->unsigned();
            $table->integer('metric_id')->unsigned();
            $table->string('name');
            $table->smallInteger('quantity');
            $table->decimal('price',8,2);
            $table->smallInteger('prepare_time');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('food_category_id')->references('id')->on('food_categories');
            $table->foreign('metric_id')->references('id')->on('metrics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_products');
    }
}
