<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('food_category_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('metric_id')->unsigned();
            $table->string('name');
            $table->smallInteger('quantity');
            $table->decimal('price',8,2);
            $table->smallInteger('prepare_time');
            $table->text('description');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('food_category_id')->references('id')->on('food_categories');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('metric_id')->references('id')->on('metrics');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
