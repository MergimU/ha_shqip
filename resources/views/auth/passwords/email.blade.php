@include('structure.head')
<body class="login">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="forget-form" action="" method="post" novalidate="novalidate" style="display: block;">
                    {{csrf_field()}}
                    <h3 class="font-green">Forget Password ?</h3>
                    <p> Enter your e-mail address below to reset your password. </p>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class=" control-label"><strong>E-Mail Address</strong></label>


                            <input id="email" type="email" class="form-control placeholder-no-fix" name="email" value="{{ old('email') }}" autocomplete="off" placeholder="Email" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                    </div>
                    <div class="form-actions">
                        <a href="{{asset('/admin-login')}}"  id="back-btn" class="btn green btn-outline">Back</a>
                        <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</body>