<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<head>
    @include('structure.head')
    <title>Ha SHQIP @yield('title')</title>
    @yield('page-styles')
</head>

<body>
<noscript><meta http-equiv="refresh" content="1;url={{asset('without-javascript')}}"></noscript>
@include('structure.header')
<div class="clearfix"></div>


<div class="page-container">
    <div class="page-sidebar-wrapper">
        @include("structure.sidebar")
    </div>
    <!-- Modal -->
@include('structure.modal')
<!-- Content -->

    <div class="page-content-wrapper">
        <div class="page-content">
                  <div class="container"style="margin-top: 40px;">
                      @yield("content")
                  </div>
        </div>
    </div>
</div>

</body>

<footer>
    @include("structure.footer")
    @include("structure.scripts")
    @yield("page-scripts")
</footer>
