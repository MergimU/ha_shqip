<div class="modal fade in" id="global-modal" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div id="message_area">
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>