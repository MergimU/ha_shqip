<script src="{{asset("assets/global/plugins/respond.min.js")}}"></script>
<script src="{{asset("assets/global/plugins/excanvas.min.js")}}"></script>
<script src="{{asset("assets/global/plugins/ie8.fix.min.js")}}"></script>

<!-- BEGIN CORE PLUGINS -->

<script src="{{asset("assets/global/plugins/jquery.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/js.cookie.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/bootstrap-select-1.12.2/js/bootstrap-select.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/pikaday-master/pikaday.js")}}" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset("assets/global/plugins/jquery-ui/jquery-ui.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/global/plugins/moment.min.js")}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/toastr/toastr.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"
        type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset("assets/global/scripts/app.min.js")}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset("assets/apps/scripts/calendar.min.js")}}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{asset("assets/layouts/layout4/scripts/layout.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/layouts/global/scripts/quick-sidebar.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/layouts/global/scripts/quick-nav.min.js")}}" type="text/javascript"></script>

<!-- BEGIN GLOBAL CUSTOM SCRIPT -->
<script src="{{asset("assets/global/scripts/custom-script.js")}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js" type="text/javascript"></script>




<!-- END GLOBAL CUSTOM SCRIPT -->
{{--<script src="../assets/pages/scripts/login.min.js" type="text/javascript"></script>--}}


