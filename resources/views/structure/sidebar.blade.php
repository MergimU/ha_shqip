<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper hidden-xs">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search" action="extra_search.html" method="POST">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <li class=" {{Request::is('admin')?"start active":""}}">
                <a href="{{asset('admin')}}">
                    <i class="icon-home font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('dashboard','Armaturenbrett')
                    }}</span>
                    <span class="selected"></span>
                </a>
            </li>

            @if(Auth::user()->isRole('superadmin'))

                {{--Begin of companies list menu--}}
            <li class=" {{Request::is('company')?"start active":""}}">
                <a href="{{asset('/company')}}">
                    <i class="icon-paper-plane font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('company','firmen')}}</span>
                    <span class="selected"></span>
                </a>
            </li>
                {{--End of companies list menu--}}


                {{--Begin of users list menu--}}
            <li class="nav-item {{Request::is('role')?" active":""}}
            {{Request::is('partner')?" active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('users','benutzer')}}</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    <li class="nav-item {{Request::is('role')?"start active":""}}">
                        <a href="{{asset('/role')}}" class="nav-link ">
                            <i class="fa fa-male font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('role','firmen')
                            }}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('partner')?"start active":""}}">
                        <a href="{{asset('/partner')}}" class="nav-link ">
                            <i class="fa fa-user-plus font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('partner','partner')
                            }}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                </ul>
            </li>
                {{--End of users list menu--}}


                {{--Begin of metric list menu--}}
            <li class=" {{Request::is('metric')?"start active":""}}">
                <a href="{{asset('/metric')}}">
                    <i class="fa fa-hourglass-end font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('metric','metriken')
                            }}</span>
                    <span class="selected"></span>
                </a>
            </li>
                {{--End of metric list menu--}}

                {{--Begin of food-category list menu--}}
                <li class=" {{Request::is('food-category')?"start active":""}}">
                    <a href="{{asset('/food-category')}}">
                        <i class="fa fa-cutlery font-green"></i>
                        <span class="title">{{App\Models\Language::translateParagraph('food_category',
                        'lebensmittelkategorie')}}</span>
                        <span class="selected"></span>
                    </a>
                </li>
                {{--End of food-category list menu--}}

                {{--Begin of Cuisine list menu--}}
            <li class=" {{Request::is('cuisine')?"start active":""}}">
                <a href="{{asset('/cuisine')}}">
                    <i class="fa fa-cutlery font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('cuisine','küche')
                            }}</span>
                    <span class="selected"></span>
                </a>
            </li>
                {{--End of cuisine menu--}}


                {{--Begin of subscription list menu--}}
            <li class="nav-item {{Request::is('subscription')?" active":""}}
            {{Request::is('subscription-category')?" active":""}}
            {{Request::is('subscription-package')?" active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-money font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('subscription','mitgliedsbeitrag')
                            }}</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>

                <ul class="sub-menu" >
                    </li>
                    <li class="nav-item {{Request::is('subscription-category')?"start active":""}}">
                        <a href="{{asset('/subscription-category')}}" class="nav-link ">
                            <i class="fa fa-list font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('subscription_category',
                            'kategorie')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('subscription-package')?"start active":""}}">
                        <a href="{{asset('/subscription-package')}}" class="nav-link ">
                            <i class="fa fa-cart-plus font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('package',
                            'paket')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                    <li class="nav-item {{Request::is('subscription')?"start active":""}}">
                        <a href="{{asset('/subscription')}}" class="nav-link ">
                            <i class="fa fa-money font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('subscriptions',
                            'abonnements')}}</span>
                            <span class="selected"></span>
                        </a>
                </ul>
            </li>
                {{--End of subscription list menu--}}


            @endif

            {{--Begin of company settings list menu--}}
            <li class="nav-item {{Request::is('settings/edit-company')?" active":""}}
            {{Request::is('deliver-detail')?"start active":""}}
            {{Request::is('translation-group')?"start active":""}}
            {{Request::is('translation')?"start active":""}}
            {{Request::is('settings/deliver-time')?"start active":""}}
            {{Request::is('settings/deliver-detail')?"start active":""}}
            {{Request::is('settings/working-hours')?"start active":""}}
            {{Request::is('settings/holidays')?"start active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gears font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('settings',
                    'einstellungen')
                    }}</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    </li>

                    <li class="nav-item {{Request::is('settings/deliver-time')?"start active":""}}
                    {{Request::is('settings/edit-company')?"start active":""}}
                    {{Request::is('settings/deliver-detail')?"start active":""}}
                    {{Request::is('settings/working-hours')?"start active":""}}
                    {{Request::is('settings/holidays')?"start active":""}}">
                        <a href="{{asset('settings/edit-company')}}" class="nav-link ">
                            <i class="fa fa-gear font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('company_settings','firmeneinstellungen')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                    @if(Auth::guard('admin')->user()->isRole('superadmin'))
                    <li class="nav-item {{Request::is('translation-group')?"start active":""}}">
                        <a href="{{asset('/translation-group')}}" class="nav-link ">
                            <i class="fa fa-object-group font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('translation_group',
                            'Übersetzungs Gruppe')}}</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    @endif


                    <li class="nav-item {{Request::is('translation')?"start active":""}}">
                        <a href="{{asset('/translation')}}" class="nav-link ">
                            <i class="fa fa-language font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('translations',
                            'Übersetzungen')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>
                </ul>
            </li>
            {{--End of company settings list menu--}}


            {{--Start of general-product list memu--}}
            @if(Auth::guard('admin')->user()->isRole('superadmin'))
            <li class="nav-item {{Request::is('general-product-ingredient')?" active":""}}
            {{Request::is('general-product')?"start active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-angle-double-right font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('general_products',
                            'Allgemeine Produkte')}}</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >


                    <li class="nav-item {{Request::is('general-product-ingredient')?"start active":""}}">
                        <a href="{{asset('/general-product-ingredient')}}" class="nav-link ">
                            <i class="fa fa-th-large font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('ingredients',
                            'Zutaten')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>


                    <li class="nav-item {{Request::is('general-product')?"start active":""}}">
                        <a href="{{asset('/general-product')}}" class="nav-link ">
                            <i class="fa fa-angle-double-right font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('general_products',
                            'Allgemeine Produkte')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>


                </ul>
            </li>
            {{--End of general-product list--}}
            @endif




            {{--Start of product list memu--}}
            <li class="nav-item {{Request::is('recommendation')?" active":""}}
            {{Request::is('product')?"start active":""}}{{Request::is('create-menu')?"start active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-th-list font-green"></i>
                    <span class="title">{{App\Models\Language::translateParagraph('company_products',
                            'firmenprodukte')}}</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    @if(Auth::guard('admin')->user()->isRole('admin'))

                        <li class="nav-item {{Request::is('create-menu')?"start active":""}}">
                            <a href="{{asset('/create-menu')}}" class="nav-link ">
                                <i class="fa fa-file-text font-green" aria-hidden="true"></i>
                                <span class="title">{{App\Models\Language::translateParagraph('create_menu',
                            'Erstellen Sie Ihr Menü')}} </span>
                                <span class="selected"></span>
                            </a>
                        </li>

                    @endif

                    <li class="nav-item {{Request::is('recommendation')?"start active":""}}">
                        <a href="{{asset('/recommendation')}}" class="nav-link ">
                            <i class="fa fa-beer font-green"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('recommendations',
                            'Empfehlungen')}}</span>
                            <span class="selected"></span>
                        </a>
                    </li>

                    {{--<li class="nav-item {{Request::is('ingredient')?"start active":""}}">
                        <a href="{{asset('/ingredient')}}" class="nav-link ">
                            <i class="fa fa-th-large font-green"></i>
                            <span class="title">Ingredients</span>
                            <span class="selected"></span>
                        </a>

                    </li>--}}

                    {{--<li class="nav-item {{Request::is('product')?"start active":""}}">
                        <a href="{{asset('/product')}}" class="nav-link ">
                            <i class="fa fa-pie-chart font-green" aria-hidden="true"></i>
                            <span class="title">Products of System</span>
                            <span class="selected"></span>
                        </a>
                    </li>--}}



                    <li class="nav-item {{Request::is('product')?"start active":""}}">
                        <a href="{{asset('/product')}}" class="nav-link ">
                            <i class="fa fa-pie-chart font-green" aria-hidden="true"></i>

                            @if(Auth::user()->isRole('superadmin'))
                            <span class="title">Partner Products</span>
                            @else
                                <span class="title">Your Products</span>
                            @endif

                            <span class="selected"></span>
                        </a>
                    </li>



                    <li class="nav-item {{Request::is('availability')?"start active":""}}">
                        <a href="{{asset('/availability')}}" class="nav-link ">
                            <i class="fa fa-check font-green" aria-hidden="true"></i>
                            <span class="title">{{App\Models\Language::translateParagraph('availability',
                            'verfügbarkeit')}}</span>
                            <span class="selected"></span>
                        </a>

                    </li>

                </ul>
            </li>
            {{--End of product list--}}



            {{--Start of order list memu--}}
            <li class="nav-item {{Request::is('')?" active":""}}
            {{Request::is('')?"start active":""}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-reorder font-green"></i>
                    <span class="title">Orders</span>
                    <span class="selected"></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    </li>

                    <li class="nav-item {{Request::is('order')?"start active":""}}">
                        <a href="{{asset('/place-order')}}" class="nav-link ">
                            <i class="fa fa-shopping-cart font-green"></i>
                            <span class="title">Place order</span>
                            <span class="selected"></span>
                        </a>

                    </li>


                </ul>
            </li>
            {{--End of order list--}}

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->