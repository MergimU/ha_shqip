@include('structure.head')
<title>Admin login page</title>
<body class="login">
<!-- END LOGO -->
<!-- BEGIN LOGIN -->

</div>
<div class="content">
    <!-- BEGIN LOGIN FORM -->

    <form class="login-form" action="{{ route('admin.login.submit') }}" method="post">
        {{csrf_field()}}
        <h3 class="form-title font-green">Sign In</h3>
    @if (count($errors) > 0)
        <div class="alert alert-info">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>
    @endif

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" value="{{old('email')}}" name="email" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
        <div class="form-actions">
            <input type="submit" class="btn green uppercase" value="Login"></input>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember
                <span></span>
            </label>
            <a href="{{asset('/password/reset')}}" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>
    </form>
</div>
<div class="copyright"> ADMIN PAGE </div>
</body>
@include('structure.scripts')