<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-hourglass-end font-green"></i>{{App\Models\Language::translateParagraph
                    ('metric','metriken')}} </div>

                <div class="actions">
                        <a href="javascript:void(0);"  id="create-metric" data-toggle="tooltip" data-placement="left" title="Add new metric" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-metric"></a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th> Name </th>

                        </tr>
                        </thead>
                        <tbody id="show-companies">

                        <div class="row">
                            @foreach($metrics as $metric)
                                <tr>
                                    <td class="col-md-10 "> {{$metric->name}} </td>
                                    <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-metric" data-id="{{$metric->id}}"></a>

                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-metric"         data-id="{{$metric->id}}"></a>

                                    </td>
                                </tr>
                            @endforeach
                        </div>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="text-center">
            {{$metrics->links()}}
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
