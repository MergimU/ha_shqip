@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/metric-style.css')}}">
@endsection
@section('title',' | Metrics')
@section('content')

    @include('admin.metrics.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/metric-script.js')}}"></script>
@endsection