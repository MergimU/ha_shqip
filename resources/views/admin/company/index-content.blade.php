<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
<i class="fa fa-comments font-green"></i>{{App\Models\Language::translateParagraph('company','firmen')}}</div>

<div class="actions">
    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new company"
       class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default company-create-js">
    </a>

</div>

<div class="portlet-body" >
    <div class="table-scrollable">
        <table class="table table-striped table-hover "  >
            <thead>
            <tr>

                <th>  Name </th>
                <th> Location </th>
                <th> Zip </th>
                <th></th>

            </tr>
            </thead>
            <tbody id="show-companies">
            <div class="row">
                @foreach($companies as $company)
                    <tr>
                        <td class="col-md-4"> {{$company->name}} </td>
                        <td class="col-md-3"> {{$company->location}} </td>
                        <td class="col-md-1"> {{$company->zip}} </td>
                        <td class="col-md-2 text-center action-anchors">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-company"
                               data-id="{{$company->id}}"></a>

                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash
                                js-delete-company" data-id="{{$company->id}}"></a>
                        </td>
                    </tr>
                @endforeach
            </div>

            </tbody>
        </table>

    </div>
</div>
</div>
<div class="text-center">
    {{$companies->links()}}
</div>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
