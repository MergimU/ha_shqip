@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/company.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-fileinput.css')}}">
    @endsection

@section('title',' | Company')
@section('content')

    @include('admin.company.index-content')

    @endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/company-script.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/scripts/bootstrap-fileinput.js')}}"></script>
    @endsection