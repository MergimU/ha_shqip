@if($data)
    <form  method="post" class="form-horizontal js-update-company" action="{{asset('company/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('company')}}" class="form-horizontal js-create-company"
                  enctype="multipart/form-data"  method="post">
@endif
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-sm-4 control-label" for="firstname">Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="name" name="name"
                       @if($data) value="{{$data->name}}" @endif required/>
                <div id="name-error" class="error font-re small error-js"></div>
            </div>

        </div>

                @if($data)
        <div class="form-group">
            <label class="col-sm-4 control-label" for="lastname">Location</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="location" name="location"
                       @if($data)  value="{{$data->location}}" @endif placeholder="name"required />
                <div id="location-error" class="error font-re small error-js"></div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="username">Zip Code</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="zip" name="zip"
                       @if($data) value="{{$data->zip}}" @endif  placeholder="zip code" required/>
                <div id="zip-error" class="error font-re small error-js"></div>
            </div>
        </div>

                @if((Auth::user()->isRole('admin')))

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="username">Minimum order</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="min_order"
                                   name="min_order"
                                   @if($data) value="{{$data->min_order}}" @endif
                                   placeholder="Minimum order free delivery" required/>
                            <div id="zip-error" class="error font-re small error-js"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="username">Max Distance
                            Delivery</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="max_distance"
                                   name="max_distance"
                                   @if($data) value="{{$data->max_distance_delivery}}" @endif
                                   placeholder="Max distance free delivery" required/>
                            <div id="zip-error" class="error font-re small error-js"></div>
                        </div>
                    </div>


                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="username">Upload logo</label>
                            <div class="col-sm-5">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="form-control uneditable-input input-fixed input-medium"
                                             data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file">
                                        <span class="fileinput-new"> Select file</span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="hidden" value="" name="..."><input type="file" name="image">
                                    </span>
                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists"
                                           data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>

                                <div id="zip-error" class="error font-re small error-js"></div>
                            </div>
                        </div>
                    @endif
                @endif

        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <button type="submit" name="superadmin-save" class="btn btn-primary btn-block submit-js"
                        value="Save">Save</button>
            </div>
        </div>
    </form>



