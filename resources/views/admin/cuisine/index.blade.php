@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/cuisine-style.css')}}">
@endsection
@section('title',' | Cuisine')
@section('content')

    @include('admin.cuisine.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/cuisine-script.js')}}"></script>
@endsection