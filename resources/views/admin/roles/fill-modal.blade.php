@if($data)
    <form method="post" class="form-horizontal js-update-role" action="{{asset('role/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('role')}}" class="form-horizontal js-role-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="name" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="role_key">Role key</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="role_key" name="role_key"
                               @if($data) value="{{$data->role_key}}" @endif placeholder="key of the role" required/>
                        <div id="role_key" class="error font-re small error-js"></div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>
