<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                 <i class="fa fa-male font-green"></i>{{App\Models\Language::translateParagraph('role','firmen')
                 }}</div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new role" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-role">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                                <th> Name </th>
                                <th>Role Key</th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($roles as $role)
                                    <tr>
                                        <td class="col-md-5"> {{ucfirst($role->name)}} </td>
                                        <td class="col-md-5"> {{$role->role_key}} </td>
                                        <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-role" data-id="{{$role->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-role"         data-id="{{$role->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <div class="text-center">
                {{$roles->links()}}
            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>


