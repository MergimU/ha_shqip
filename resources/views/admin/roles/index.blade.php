@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/role-style.css')}}">
@endsection
@section('title',' | Roles')
@section('content')

    @include('admin.roles.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/role-script.js')}}"></script>
@endsection