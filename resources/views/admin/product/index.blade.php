@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/product-style.css')}}">
@endsection
@section('title',' | Products')
@section('content')

    @include('admin.product.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/product-script.js')}}"></script>
@endsection