
@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/product-style.css')}}">
@endsection
@section('title',' | Partners')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" >
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-th-list font-green"></i>Partners </div>

                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>

                                <tr>
                                    <th>Partner</th>
                                    <th> Number of products </th>
                                </tr>

                                </thead>
                                <tbody id="show-companies">

                                <div class="row">
                                    @foreach($partners as $partner)
                                        <tr>
                                            @if(Auth::guard('admin')->user()->isRole('superadmin'))
                                                <td >{{$partner->name}}</td>
                                            @endif
                                            <td class="col-md-2 text-center">
                                                <a href="{{asset('product').'?company_id='
                                                .$partner->id}}" id="products-number">
                                                    {{count($partner->products)}}
                                                </a>
                                            </td>


                                        </tr>
                                    @endforeach
                                </div>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="text-center">

                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/product-script.js')}}"></script>
@endsection



