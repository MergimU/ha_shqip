<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-th-list font-green"></i>Products </div>

                @if(Auth::guard('admin')->user()->isRole('superadmin'))
                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new product" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-product">
                    </a>
                </div>
                @endif
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                                <th> Name </th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Ingredients</th>
                                <th>Recommendation</th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($products as $product)
                                    <tr>

                                        <td ><a href="javascript:;void(0)">{{ucfirst($product->name)}}</a>
                                        </td>
                                        <td > {{$product->category->name}} </td>
                                        <td> {{$product->price}} </td>
                                        <td > @foreach($product->ingredients as $ingredient)
                                                <span class="badge">{{$ingredient->name}}</span>
                                                  @endforeach
                                        </td>

                                        <td>

                                            @foreach($product->recommendations as $recommendation)
                                                <div class="badge">{{ucfirst($recommendation->name)}}</div>
                                                @endforeach
                                        </td>
                                        <td class=" text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                               title="Edit" class="btn btn-sm glyphicon glyphicon-edit
                                               js-edit-product" data-id="{{$product->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                               title="Delete" class="btn delete-anchor  btn-sm glyphicon
                                               glyphicon-trash
                                               js-delete-product" data-id="{{$product->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">

            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
