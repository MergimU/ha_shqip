@if($data)
    <form  method="post" class="form-horizontal js-update-availability" action="{{asset('availability/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('availability')}}" class="form-horizontal js-create-availability"  method="post">
@endif
        {{csrf_field()}}

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="product">Choose Product</label>
                    <div class="col-sm-5 ">
                        <select name="product"  class="selectpicker form-control show-tick"
                                title="Choose product" id="product" data-live-search="true">
                            @foreach($products as $product)
                                <option @if($data && $product->id == $data->product_id)
                                        selected @endif
                                        value="{{$product->id}}">  {{$product->name}}
                                </option>
                            @endforeach
                        </select>
                        <div id="product-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="day">Choose day</label>
                    <div class="col-sm-5 ">
                        <select name="day" class="selectpicker form-control show-tick"
                                title="Choose day" id="day">

                            <option value="1">{{ucfirst('monday') }}</option>
                            <option value="2">{{ucfirst('tuesday')}}</option>
                            <option value="3">{{ucfirst('wednesday')}}</option>
                            <option value="4">{{ucfirst('thursday')}}</option>
                            <option value="5">{{ucfirst('friday')}}</option>
                            <option value="6">{{ucfirst('saturday')}}</option>
                            <option value="7">{{ucfirst('sunday')}}</option>

                        </select>
                        <div id="day-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="start_at">Start at</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" name="start" class="form-control timepicker timepicker-24"
                                   id="start_at">
                            <span class="input-group-btn" >
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"></i></button>
                            </span>
                        </div>
                        <div id="start-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="end_at">End at</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker timepicker-24" name="end" id="end_at">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"></i></button>
                            </span>
                        </div>
                        <div id="end-error" class="error font-re small error-js"></div>
                    </div>
                </div>


        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <button type="submit" name="superadmin-save" class="btn btn-primary btn-block submit-js"
                        value="Save">Save</button>
            </div>
        </div>
    </form>



