@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/availability-style.css')}}">
    @endsection

@section('title',' | Availabilities')
@section('content')

    @include('admin.availabilities.index-content')

    @endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/availability-script.js')}}"></script>
    @endsection