<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
<i class="fa fa-check font-green"></i>Availabilities of specific product </div>

<div class="actions">
    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new availabilities"
       class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default availability-create-js">
    </a>

</div>

<div class="portlet-body" >
    <div class="table-scrollable">
        <table class="table table-striped table-hover "  >
            <thead>
            <tr>

                <th> Product </th>
                <th> Days </th>
                <th> Start At </th>
                <th> End At </th>
                <th></th>

            </tr>
            </thead>
            <tbody id="show-companies">
            <div class="row">
                @foreach($availabilities as $availability)
                    <tr>
                        <td class="col-md-4"> {{ucfirst($availability->product->name)}}</td>
                        <td class="col-md-3"> {{ucfirst($availability->days)}} </td>
                        <td class="col-md-1"> {{date('H:i',strtotime($availability->start_at))}} </td>
                        <td class="col-md-1"> {{date('H:i',strtotime($availability->end_at))}} </td>
                        <td class="col-md-2 text-center action-anchors">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-availability" data-id="{{$availability->id}}"></a>

                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"
                               class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-availability"                                                                data-id="{{$availability->id}}"></a>

                        </td>
                    </tr>
                @endforeach
            </div>

            </tbody>
        </table>

    </div>
</div>
</div>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
