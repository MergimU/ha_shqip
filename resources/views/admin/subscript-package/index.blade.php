@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/subscript-package-style.css')}}">
@endsection
@section('title',' | Subscript Package')
@section('content')

    @include('admin.subscript-package.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/subscript-package-script.js')}}"></script>
@endsection