<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cart-plus font-green"></i>{{App\Models\Language::translateParagraph('package',
                            'paket')}} </div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new subscription package" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-sub-package">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                            <th> Type of Subscritpion </th>
                            <th> Price/Percentage </th>
                            <th> Subscription Category </th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($packages as $package)
                                    <tr>
                                        <td> {{$package->name}} </td>
                                        <td > {{$package->price}} </td>
                                        <td > {{$package->category->name}} </td>
                                        <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-sub-package" data-id="{{$package->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-sub-package"         data-id="{{$package->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">
                {{$packages->links()}}
            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
