@if($data)
    <form method="post" class="form-horizontal js-update-sub-package" action="{{asset('subscription-package/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('subscription-package')}}"  class="form-horizontal js-sub-package-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Type of subscription</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="type of subscription" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>
                </div>




                <div class="form-group">
                    <label class="col-sm-4 control-label" for="category">Subscript category</label>
                    <div class="col-sm-5 ">
                        <select name="category_id" id="category"  class="selectpicker form-control show-tick"  title="Choose subscript category">
                            @foreach($categories as $category)

                                <option @if($data && $category->id == $data->sub_cat_id)  selected @endif                                                                                 value="{{$category->id}}">  {{$category->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="category-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="price">Price/Percentage</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="price" name="price"
                               @if($data) value="{{$data->price}}" @endif placeholder="price/percentage" required/>
                        <div id="price-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>
