@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/partner-style.css')}}">
@endsection
@section('title',' | Partners')
@section('content')

    @include('admin.partners.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/partner-script.js')}}"></script>
@endsection