@if($data)
    <form method="post" class="form-horizontal js-update-partner" action="{{asset('partner/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('partner')}}"  class="form-horizontal js-partner-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="name" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Surname</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="surname" name="surname"
                               @if($data) value="{{$data->surname}}" @endif placeholder="surname" required/>
                        <div id="surname-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="email" name="email"
                               @if($data) value="{{$data->email}}" @endif placeholder="email" required/>
                        <div id="email-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="password"   @if(!$data) required @endif/>
                        <div id="password-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="firstname">Confirm Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="confirm_password"
                               name="confirm_password" placeholder="confirm password" @if(!$data) required @endif/>
                        <div id="confirm-password-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cuisines">Choose company</label>
                    <div class="col-sm-5 ">
                        <select name="company_id"  class="selectpicker form-control show-tick"
                                title="Choose company" data-live-search="true">
                            @foreach($companies as $company)
                                <option @if($data && $company->id == $data->company_id)
                                        selected @endif
                                value="{{$company->id}}">  {{$company->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="company-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cuisines">Choose roles</label>
                    <div class="col-sm-5 ">
                        <select id="select-roles"  class="selectpicker form-control show-tick
                         partner-role" name="role_id"
                                multiple ="multiple"  title="Choose roles">
                            @foreach($roles as $role)
                                <option @if($data) @foreach($data->roles as $obj) @if($data &&
                                $role->id ==$obj->id) selected @endif @endforeach @endif
                                value="{{$role->id}}">  {{$role->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="role-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>

