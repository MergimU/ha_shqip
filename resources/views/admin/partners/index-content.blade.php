<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users font-green"></i>{{App\Models\Language::translateParagraph('partner',
                    'partner')}}</div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new
                    partner" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                    js-create-partner">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                                <th> Name </th>
                                <th> Surname </th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Role</th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($partners as $partner)
                                    <tr>
                                        <td > {{$partner->name}} </td>
                                        <td> {{$partner->surname}} </td>
                                        <td> {{$partner->email}} </td>
                                        <td > {{$partner->company->name}} </td>
                                        <td >
                                       @foreach($partner->roles as $role)
                                            <label class="badge">{{$role->name}}</label>
                                           @endforeach
                                        </td>
                                        <td class=" text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Edit" class="btn btn-sm glyphicon
                                               glyphicon-edit js-edit-partner" data-id="{{$partner->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                                btn-sm glyphicon glyphicon-trash js-delete-partner"
                                               data-id="{{$partner->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">
                {{$partners->links()}}
            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
