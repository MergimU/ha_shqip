<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-green"></i>{{App\Models\Language::translateParagraph('subscriptions',
                            'abonnements')}}</div>

                <div class="actions">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="left"
                       title="Add new subscription"
                       class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-subscription">
                    </a>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                            <th> Company Name </th>
                            <th> Package </th>
                            <th> Category </th>
                            <th> Started </th>
                            <th>Ended</th>
                            <th>Status</th>


                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($subscriptions as $subscription)
                                    <tr>
                                        <td> {{$subscription->company->name}} </td>
                                        <td> {{$subscription->package->name}} </td>
                                        <td> {{$subscription->package->category->name}} </td>
                                        <td> {{ Carbon\Carbon::parse($subscription->started_at)
                                        ->toFormattedDateString() }} </td>
                                        <td> {{ Carbon\Carbon::parse($subscription->ended_at)
                                        ->toFormattedDateString  () }}
                                        </td>
                                        <td>@if($subscription->status == 1)
                                                <label class="label label-success">Active</label>
                                                @else
                                                <label class="label label-danger">Expired</label>
                                                @endif
                                        </td>
                                        <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-subscription" data-id="{{$subscription->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-subscription"         data-id="{{$subscription->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">
                {{$subscriptions->links()}}
            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
