@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/subscription-style.css')}}">
@endsection
@section('title',' | Subscription')
@section('content')

    @include('admin.subscription.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/subscription-script.js')}}"></script>
@endsection