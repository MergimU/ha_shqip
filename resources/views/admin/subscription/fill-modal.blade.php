@if($data)
    <form method="post" class="form-horizontal js-update-subscription" action="{{asset('subscription/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('subscription')}}"  class="form-horizontal js-subscription-create" method="post">
                @endif
                {{csrf_field()}}

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="company">Choose company</label>
                    <div class="col-sm-5 ">
                        <select name="company_id" id="company"  class="selectpicker form-control show-tick"  title="Choose company" data-live-search="true">
                            @foreach($companies as $company)
                                <option @if($data && $company->id == $data->company_id)  selected @endif                                    value="{{$company->id}}">  {{$company->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="company-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cuisines">Choose package</label>
                    <div class="col-sm-5 ">
                        <select name="package_id"  class="selectpicker form-control show-tick"  title="Choose package">
                            @foreach($packages as $package)
                                <option @if($data && $package->id == $data->package_id)  selected @endif                                    value="{{$package->id}}">  {{$package->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="package-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                @if(!$data)
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="category">Choose category</label>
                    <div class="col-sm-5 ">
                        <select name="category_id" id="category" class="selectpicker form-control show-tick"  title="Choose category">
                            @foreach($categories as $category)
                                <option @if($data && $category->id ==  $data->package->category->id)  selected
                                        @endif
                                        value="{{$category->id}}">  {{$category->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="category-error" class="error font-re small error-js"></div>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="datepicker">Started date</label>
                    <div class="col-sm-5 ">
                                <div class="input-group input-medium date date-picker"                                            data-date-format="dd-mm-yyyy"   data-date-start-date="+0d">
                                    <input type="text" name="started"  class="form-control" readonly=""id="datepicker" >
                                    <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <div id="started-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="datepicker">Ended date</label>
                    <div class="col-sm-5 ">
                        <div class="input-group input-medium date date-picker"                                                              data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                            <input type="text" name="ended"  class="form-control" readonly=""  id="datepicker">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <div id="ended-error" class="error font-re small error-js"></div>
                    </div>
                </div>




                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>