@if($data)
    <form  method="post" class="form-horizontal js-update-translation" action="{{asset('translation/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('translation')}}" class="form-horizontal js-create-translation"
                  enctype="multipart/form-data"  method="post">
@endif
        {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="language">Choose Language</label>
                    <div class="col-sm-5 ">
                        <select name="language"  class="selectpicker form-control show-tick"  title="Choose                                  language">
                            @foreach($languages as $language)
                                <option @if($data && $language->id == $data->language_id)  selected @endif                                    value="{{$language->id}}">  {{ucfirst($language->name)}}
                                </option>

                            @endforeach
                        </select>
                        <div id="language-error" class="error font-re small error-js"></div>
                    </div>
                </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="key">Key </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="key" name="key"
                       @if($data) value="{{$data->key}}" @endif required/>
                <div id="key-error" class="error font-re small error-js"></div>
            </div>

        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="translation">Translation </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="translation" name="translation"
                       @if($data) value="{{$data->translation}}" @endif required/>
                <div id="translation-error" class="error font-re small error-js"></div>
            </div>

        </div>


        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <button type="submit" name="superadmin-save" class="btn btn-primary btn-block submit-js"
                        value="Save">Save</button>
            </div>
        </div>
    </form>



