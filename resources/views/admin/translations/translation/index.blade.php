@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/translate-style.css')}}">
    @endsection

@section('title',' | Translation Group')
@section('content')

    @include('admin.translations.translation.index-content')

    @endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/translate-script.js')}}"></script>
    @endsection