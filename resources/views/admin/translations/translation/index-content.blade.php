<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
<i class="fa fa-language font-green"></i>{{App\Models\Language::translateParagraph('translations',
                            'Übersetzungen')}}</div>

<div class="actions">
    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new translation  "
       class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default translation-create-js">
    </a>

</div>

<div class="portlet-body" >
    <div class="table-scrollable">
        <table class="table table-striped table-hover "  >
            <thead>
            <tr>

                <th> Language </th>
                <th> Key </th>
                <th> Translation </th>


            </tr>
            </thead>
            <tbody id="show-companies">
            <div class="row">
                @foreach($translations as $translation)
                    <tr>
                        <td > {{ucfirst($translation->language->name)}} </td>
                        <td > {{$translation->key}} </td>
                        <td > {{ucfirst($translation->translation)}} </td>
                        <td class="text-right action-anchors">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-translation"
                               data-id="{{$translation->id}}"></a>

                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash
                                js-delete-translation" data-id="{{$translation->id}}"></a>
                        </td>
                    </tr>
                @endforeach
            </div>

            </tbody>
        </table>

    </div>
</div>
</div>
<div class="text-center">
    {{$translations->links()}}
</div>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
