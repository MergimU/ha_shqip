@if($data)
    <form  method="post" class="form-horizontal js-update-translation-group" action="{{asset('translation-group/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('translation-group')}}" class="form-horizontal js-create-translation-group"
                  enctype="multipart/form-data"  method="post">
@endif
        {{csrf_field()}}
        <div class="form-group">
            <label class="col-sm-4 control-label" for="entity">Entity Name</label>
            <div class="col-sm-5">
                <input type="text" class="form-control" id="entity" name="entity"
                       @if($data) value="{{$data->entity}}" @endif required/>
                <div id="entity-error" class="error font-re small error-js"></div>
            </div>

        </div>


        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <button type="submit" name="superadmin-save" class="btn btn-primary btn-block submit-js"
                        value="Save">Save</button>
            </div>
        </div>
    </form>



