<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
<i class="fa fa-object-group font-green"></i>Translation Group </div>

<div class="actions">
    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new translation group "
       class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default translation-group-create-js">
    </a>

</div>

<div class="portlet-body" >
    <div class="table-scrollable">
        <table class="table table-striped table-hover "  >
            <thead>
            <tr>

                <th> Entity </th>


            </tr>
            </thead>
            <tbody id="show-companies">
            <div class="row">
                @foreach($translation_groups as $translation_group)
                    <tr>
                        <td > {{ucfirst($translation_group->entity)}} </td>
                        <td class="text-right action-anchors">
                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-translation-group"
                               data-id="{{$translation_group->id}}"></a>

                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                               title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash
                                js-delete-translation-group" data-id="{{$translation_group->id}}"></a>
                        </td>
                    </tr>
                @endforeach
            </div>

            </tbody>
        </table>

    </div>
</div>
</div>
<div class="text-center">
    {{$translation_groups->links()}}
</div>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
