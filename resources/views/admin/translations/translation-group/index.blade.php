@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/translation-group-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-fileinput.css')}}">
    @endsection

@section('title',' | Translation Group')
@section('content')

    @include('admin.translations.translation-group.index-content')

    @endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/translation-group-script.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/scripts/bootstrap-fileinput.js')}}"></script>
    @endsection