<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">

                    <i class="fa fa-cutlery font-green"></i>{{App\Models\Language::translateParagraph
                    ('food_category','lebensmittelkategorie')}}</div>

                <div class="actions">
                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Add new category of food" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-food-category">
                    </a>

                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover ">
                        <thead>
                        <tr>

                            <th> Name </th>
                            <th>Cuisine</th>

                        </tr>
                        </thead>
                        <tbody id="show-companies">

                        <div class="row">
                            @foreach($categories as $category)
                                <tr>
                                    <td class="col-md-5"> {{$category->name}} </td>
                                    <td class="col-md-5"> {{$category->cuisine->name}}</td>
                                    <td class="col-md-2 text-center action-anchors">
                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-food-category" data-id="{{$category->id}}"></a>

                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-food-category" data-id="{{$category->id}}"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </div>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="text-center">
            {{$categories->links()}}
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
