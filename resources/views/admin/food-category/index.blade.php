@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/food-category-style.css')}}">
@endsection
@section('title',' | Food Categories')
@section('content')

    @include('admin.food-category.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/food-category-script.js')}}"></script>
@endsection