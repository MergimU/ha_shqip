<div id="product-images">
    <ul>
        @foreach($product_images as $product_image)
            <li @if($product_image->main == 1) class="image-main" @endif>
                <a href="{{asset('storage/product-images/'.$product_image->img_path)}}"
                   data-lightbox="product_image"
                   data-title="{{$product->name}}">
                    <img src="{{asset('storage/product-thumbnails/'.$product_image->img_path)}}" alt="">
                </a>

                <div class="make-main-delete">
                    <a href="javascript:;void(0)" type="button" class="btn btn-circle dark
                 btn-sm" data-id="{{$product_image->id}}" id="make-main"> Make main </a>

                    <a href="javascript:;void(0)" data-toggle="tooltip"                                                     data-id="{{$product_image->id}}" title="Delete Image"
                       class="remove-image"data-placement="bottom">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>

                <input type="hidden" class="product" name="product" value="{{$product->id}}">

            </li>
        @endforeach
    </ul>
</div>
