@if($data)
    <form method="post" class="form-horizontal js-update-general-product" action="{{asset('general-product/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('general-product')}}"  class="form-horizontal js-general-product-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="name" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="quantity">Quantity</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="quantity" name="quantity"
                               @if($data) value="{{$data->quantity}}" @endif placeholder="quantity" required/>
                        <div id="quantity-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="prepare_time">Prepare time</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="prepare_time" name="prepare_time"
                               @if($data) value="{{$data->prepare_time}}" @endif placeholder="prepare time"
                               required/> <div id="prepare_time-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cuisines">Choose Category</label>
                    <div class="col-sm-5 ">
                        <select name="categories"  class="selectpicker form-control show-tick"
                                title="Choose category" data-live-search="true">
                            @foreach($categories as $category)
                                <option @if($data && $category->id == $data->food_category_id)
                                        selected @endif
                                value="{{$category->id}}">  {{$category->name}}
                                </option>
                            @endforeach
                        </select>
                        <div id="category-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="ingredients">Choose cuisines</label>
                    <div class="col-sm-5 ">
                        <select id="cuisine"  class="selectpicker form-control show-tick
                         partner-role" name="cuisine"
                                multiple ="multiple"  title="Choose cuisine">
                            @foreach($cuisines as $cuisine)
                                <option @if($data) @foreach($data->cuisines as $obj) @if($data &&
                                $cuisine->id ==$obj->id) selected @endif @endforeach @endif
                                value="{{$cuisine->id}}">  {{$cuisine->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="cuisine-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="cuisines">Choose metric</label>
                    <div class="col-sm-5 ">
                        <select name="metric"  class="selectpicker form-control show-tick"
                                title="Choose metric" data-live-search="true">
                            @foreach($metrics as $metric)
                                <option @if($data && $metric->id == $data->metric_id)
                                        selected @endif
                                        value="{{$metric->id}}">  {{$metric->name}}
                                </option>
                            @endforeach
                        </select>
                        <div id="metric-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="price">Price</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="price" name="price"
                               @if($data) value="{{$data->price}}" @endif placeholder="price" required/>
                        <div id="price-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="ingredients">Ingredients</label>
                    <div class="col-sm-5 ">
                        <select id="ingredients"  class="selectpicker form-control show-tick
                         partner-role" name="ingredients"
                                multiple ="multiple"  title="Choose ingredients">
                            @foreach($ingredients as $ingredient)
                                <option @if($data) @foreach($data->ingredients as $obj) @if($data &&
                                $ingredient->id ==$obj->id) selected @endif @endforeach @endif
                                value="{{$ingredient->id}}">  {{$ingredient->name}}
                                </option>

                            @endforeach
                        </select>
                        <div id="ingredients-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>
