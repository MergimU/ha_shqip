@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/global/plugins/dropzone/min/dropzone.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/dropzone/min/basic.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/lightbox/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/styles/product-images-style.css')}}">
@endsection
@section('title',' | Edit Product')
@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->

                    <div class="profile-userpic">
                        @if($main_product)
                            <img src="{{asset('storage/product-thumbnails/'.$main_product)}}"
                                 class="img-responsive"
                                 alt=""> </div>
                    @else
                        <img src="{{asset('storage/product-thumbnails/no-image.jpg')}}"
                             class="img-responsive"
                             alt="">
                </div>
            @endif
            <!-- END SIDEBAR USERPIC -->


                <!-- SIDEBAR MENU -->

                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
        </div>
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <h4><strong><u>Current product info:</u></strong></h4>
                    <div class="">
                        <div class="profile-stat-text"> Product name: <span
                                    class="badge">{{$product->name}}</span> </div>
                        <div class="profile-stat-text"> Category of product: <span
                                    class="badge">{{$product->category->name}}</span></div>
                        <div class="profile-stat-text"> Cuisine:
                            @foreach($product->cuisines as $cuisine)
                            <span class="badge"> {{$cuisine->name}}</span> @endforeach </div>
                        <div class="profile-stat-text"> Metric: <span
                                    class="badge">{{$product->metric->name}}</span> </div>
                        <div class="profile-stat-text"> Quantity: <span
                                    class="badge">{{$product->quantity}}</span> </div>
                        <div class="profile-stat-text"> Prepare time: <span
                                    class="badge">@if($product->prepare_time != null) {{$product->prepare_time."
                                     minutes"}}@endif </span> </div>
                        <div class="profile-stat-text"> Ingredients:
                            @foreach($product->ingredients as $ingredient)
                            <span class="badge">{{$ingredient->name}}</span> @endforeach
                        </div>
                    </div>
                </div>
                <!-- END STAT -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>


        <div class="col-md-9">
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-green-madison bold
                                     uppercase">Images of Product</span>
                                </div>
                                <hr>
                            </div>


                                   @include('admin.general-product.edit-with-photos-ajax')


                            <form action="{{asset('product-image')}}" method="post" class="dropzone"
                                  id="productImage" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="product" value="{{$product->id}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/global/plugins/dropzone/min/dropzone.min.js')
    }}"></script>

    <script type="text/javascript" src="{{asset('assets/global/plugins/lightbox/js/lightbox.min.js')
    }}"></script>

    <script type="text/javascript" src="{{asset('assets/admin/scripts/product-images-script.js')}}"></script>
@endsection