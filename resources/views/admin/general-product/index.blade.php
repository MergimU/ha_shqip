@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/general-product-style.css')}}">
@endsection
@section('title',' | Products')
@section('content')

            @include('admin.general-product.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/general-product-script.js')}}"></script>
@endsection