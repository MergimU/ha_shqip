<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-angle-double-right font-green"></i>{{App\Models\Language::translateParagraph
                    ('general_products','Allgemeine Produkte')}}</div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new general product" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                    js-create-general-product">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                                <th>Name </th>
                                <th>Category</th>
                                <th>Metric</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Ingredients</th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($general_products as $general_product)
                                    <tr>
                                        <td ><a href="{{asset('general-product/'.$general_product->id.'/edit')
                                        }}">{{ucfirst($general_product->name)}}</a> </td>
                                        <td > {{$general_product->category->name}} </td>
                                        <td > {{$general_product->metric->name}} </td>
                                        <td > {{$general_product->quantity}} </td>
                                        <td> {{$general_product->price}} </td>
                                        <td > @foreach($general_product->ingredients as $ingredient)
                                                <span class="badge">{{$ingredient->name}}</span>@endforeach
                                        </td>
                                        {{--<td>

                                            @foreach($product->recommendations as $recommendation)
                                                <div class="badge">{{ucfirst($recommendation->name)}}</div>
                                                @endforeach
                                        </td>--}}
                                        <td class=" text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Edit" class="btn btn-sm glyphicon
                                               glyphicon-edit js-edit-general-product"
                                               data-id="{{$general_product->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash js-delete-general-product"
                                               data-id="{{$general_product->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">

            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
