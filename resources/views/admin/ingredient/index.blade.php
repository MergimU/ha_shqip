@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/ingredient-style.css')}}">
@endsection
@section('title',' | Ingredients')
@section('content')

    @include('admin.ingredient.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/ingredient-script.js')}}"></script>
@endsection