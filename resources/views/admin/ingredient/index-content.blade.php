<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-th-large font-green"></i>{{App\Models\Language::translateParagraph('ingredients',
                            'Zutaten')}}</div>

                <div class="actions">
                    <a href="javascript:void (0);" data-toggle="tooltip" data-placement="left" title="Add new ingredient" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-ingredient">
                    </a>

                </div>

            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-hover ">
                        <thead>

                            <th> Name </th>

                        </thead>
                        <tbody id="show-companies">

                        <div class="row">
                            @foreach($ingredients as $ingredient)
                                <tr>
                                    <td class="col-md-10"> {{ucfirst($ingredient->name)}} </td>
                                    <td class="col-md-2  text-center action-anchors">
                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-ingredient" data-id="{{$ingredient->id}}"></a>

                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-ingredient" data-id="{{$ingredient->id}}"></a>

                                    </td>
                                </tr>
                            @endforeach
                        </div>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="text-center">
            {{$ingredients->links()}}
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
