@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/recommendation-style.css')}}">
@endsection
@section('title',' | Recommendation')
@section('content')

    @include('admin.recommendation.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/recommendation-script.js')}}"></script>
@endsection