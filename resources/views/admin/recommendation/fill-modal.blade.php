@if($data)
    <form method="post" class="form-horizontal js-update-recommendation" action="{{asset('recommendation/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('recommendation')}}"  class="form-horizontal js-recommendation-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="name" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="price">Price</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="price" name="price"
                               @if($data) value="{{$data->price}}" @endif placeholder="price" required/>
                        <div id="price-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>

