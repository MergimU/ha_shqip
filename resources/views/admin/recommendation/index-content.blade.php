<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-beer font-green"></i>Recommendation </div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new
                    recommendation" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                    js-create-recommendation">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                                <th> Name of product </th>
                                <th> Price </th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($recommendations as $recommendation)
                                    <tr>
                                        <td > {{$recommendation->name}} </td>
                                        <td> {{$recommendation->price}} </td>
                                        <td >
                                        <td class=" text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                               title="Edit" class="btn btn-sm glyphicon glyphicon-edit
                                               js-edit-recommendation"
                                               data-id="{{$recommendation->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                               title="Delete" class="btn delete-anchor btn-sm glyphicon glyphicon-trash
                                                js-delete-recommendation" data-id="{{$recommendation->id}}"></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
