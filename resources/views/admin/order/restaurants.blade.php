@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/order-style.css')}}">
@endsection


@section('title',' | Order')
@section('content')




  <ul class="companies-list">
    @foreach($companies as $company)
     <li>
         {{--start logo of company--}}
         <div class="company-logo">
             <img src="{{asset('storage/company-images/'.$company->img_path)}}" alt="">
         </div>
         {{--end logo of company--}}



         <div class="company-info">

             {{--linked name--}}
          <a href="{{asset('order/'.$company->id)}}">{{$company->name}}</a>


             {{--Location--}}
             <p><strong>Location: </strong>{{$company->location}}
                 <i class="fa fa-map-marker" aria-hidden="true"></i>
             </p>


             {{--Delivery Time--}}
             <p><strong>Delivery Times:</strong>
                     @foreach($company->deliverTimes as $delivery) {{$delivery->start_at}}@endforeach
                 {{$company->location}}
             </p>


             <p>{{date('w')}}</p>
         </div>
         <div>
             <p><strong>Minimum order:</strong> {{$company->min_order}}</p>
         </div>
     </li>
    @endforeach
  </ul>



@endsection
@section('page-scripts')

@endsection