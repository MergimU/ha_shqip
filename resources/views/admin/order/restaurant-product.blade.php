@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/order-style.css')}}">
@endsection


@section('title',' | Order')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="product-container">
                <ul class="product-list">
                    @foreach($products as $product)
                        <li>
                            {{--<div class="product-inner">--}}
                            <span class="product-name">{{ucfirst($product->name)}}</span>
                            <span class="product-price" >{{$product->price}}</span>
                            <button class="btn btn-primary btn-order" data-id="{{$product->id}}"
                                    data-name="{{$product->name}}" data-price="{{$product->price}}">
                             <span class="fa fa-plus" >
                             </span></button>
                            {{--</div>--}}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-md-4">
        <div class="order-container">
            <div class="row">
                <div class="title-menu col-md-12">
                    <p>Orders</p>
                </div>

                <div class="ordered-product">
                    <div class="col-md-6">
                        <span class="product-name" ><strong>Name</strong></span>
                    </div>
                    <div class="col-md-4">
                        <span class="product-price"><strong>Price</strong></span>
                    </div>
                    <div class="col-md-2">
                        <span class="fa fa-remove product-remove"></span>
                    </div>
                </div>

            </div>
        </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <span id="order-total">Total: 0 CHF</span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="" class="btn btn-success fa fa-shopping-cart col-md-12"> Purchase</a>
                </div>
            </div>


        </div>
    </div>

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/order-script.js')}}"></script>
@endsection