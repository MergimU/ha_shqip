@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/order-style.css')}}">
@endsection


@section('title',' | Order')
@section('content')

<div class="row">
    <div class="jumbotron col-md-10 ">

        <form action="{{asset('restaurant/zip/{zip}')}}" method="get">
            <div class="form-group col-md-7 ">
                <label for="zip-search">Your Postal Code?</label>
                <input type="text" class="form-control" id="zip-search" name="zip-search">
            </div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-primary btn-block btn-search btn-block">
                    <i class="fa fa-search"></i>Find me a good restaurant </button>
            </div>

        </form>

    </div>
</div>
@endsection
@section('page-scripts')

@endsection