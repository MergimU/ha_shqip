@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/subscript_category-style.css')}}">
@endsection
@section('title',' | Subscript Category')
@section('content')

    @include('admin.subscript_category.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/subscript_category-script.js')}}"></script>
@endsection