<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list font-green"></i>{{App\Models\Language::translateParagraph('subscription_category','kategorie')}} </div>

                <div class="actions">
                    <a href="javascript:void(0);"  data-toggle="tooltip" data-placement="left" title="Add new category of subscription" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default js-create-sub-category">
                    </a>

                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>

                            <th> Name </th>

                            </thead>
                            <tbody id="show-companies">

                            <div class="row">
                                @foreach($sub_categories as $sub_category)
                                    <tr>
                                        <td class="col-md-10"> {{$sub_category->name}} </td>
                                        <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm  glyphicon glyphicon-edit js-edit-sub-category" data-id="{{$sub_category->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete" class="btn delete-anchor  btn-sm glyphicon glyphicon-trash js-delete-sub-category"         data-id="{{$sub_category->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="text-center">
                {{$sub_categories->links()}}
            </div>
        </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
</div>
