@extends('structure.layout')
@section('title', '| Create Menu')

@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/menu-style.css')}}">
    @endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="menu">
            <div class="general-product">
                <div class="portlet light bordered ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pie-chart font-green"></i>
                            <span class="caption-subject font-green bold uppercase">General Menu</span>
                        </div>

                        <div class="search-box-wrapper">
                            <input type="text" class="form-control search-box-input search-1"
                                   placeholder="Search for  product..." id="search-1">
                            <i class="glyphicon glyphicon-search"></i>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                       <div class="">
                           <ul class="product-list connectedSortable products-1"  id="sortable1">
                               @foreach($non_existing_general_products as $non_existing_general_product)
                                   <li data-product-id="{{$non_existing_general_product->id}}"
                                       class="product product-1">{{ucfirst
                                       ($non_existing_general_product->name)
                                       }}</li>
                                   @endforeach
                           </ul>
                       </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="menu">
                <div class="general-product">
                    <div class="portlet light bordered" >
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-pie-chart font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Your Menu</span>
                            </div>

                            <div class="search-box-wrapper">
                                <input type="text" class="form-control search-box-input search-2"
                                       placeholder="Search for  product..." id="search-2">
                                <i class="glyphicon glyphicon-search"></i>
                            </div>

                            <div class="tools">
                                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="">
                                <ul class="product-list connectedSortable products-2" id="sortable2">
                                    @foreach($existing_general_products as $existing_general_product)
                                        <li data-id="{{$existing_general_product->id}}"
                                            class="product">{{ucfirst($existing_general_product->name)
                                       }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection



@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/menu-script.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

    @endsection