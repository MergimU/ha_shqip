<!-- BEGIN CONTENT -->
@extends('structure.layout')
@section('title',' | Admin')

@section('content')
    <div class="company-picture">
        @if($company->img_path != null)
            <img src="{{asset('storage/company-images/'.$company->img_path)}}" class="img-responsive"
                 alt="">@endif
    </div>
    <p>{{App\Models\Language::translateParagraph('greeting','halo')}}</p>
    @endsection