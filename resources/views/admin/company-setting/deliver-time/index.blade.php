@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-fileinput.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/styles/delivery-times-style.css')}}">
@endsection

@section('title',' | Delivery Times')
@section('content')

    @include('admin.company-setting.deliver-time.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/global/scripts/bootstrap-fileinput.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/scripts/delivery-times-script.js')}}"></script>
@endsection