@include('admin.company-setting.setting-layout')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar font-green"></i>{{App\Models\Language::translateParagraph('delivery_schedule','Lieferungsplan')}} </div>
                <form action="">
                    <label class="mt-checkbox mt-checkbox-outline"> Does your company deliver?
                        <input type="checkbox" value="1" name="check-deliver" id="check-deliver"
                               data-company-id="{{$company->id}}" @if($company->with_deliver == 1) checked @endif>
                        <span></span>
                    </label>
                </form>
                @if($company->with_deliver == 1)
                    <div class="actions">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new
                    delivery times"
                           class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                       delivery-times-create-js">
                        </a>

                    </div>
                    @endif

                <div class="portlet-body" >
                    <div class="table-scrollable">
                        @if($company->with_deliver == 1)
                        <table class="table table-striped table-hover "  id="table-deliver-schedule">
                            <thead>
                            <tr>

                                <th> Day </th>
                                <th> Start At: </th>
                                <th> End At: </th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody id="show-companies">
                            <div class="row">
                                @foreach($delivery_times as $delivery_time)
                                    <tr>
                                        <td class="col-md-4"> {{ucfirst($delivery_time->day)}} </td>
                                        <td class="col-md-3"> {{$delivery_time->start_at}} </td>
                                        <td class="col-md-1"> {{$delivery_time->end_at}} </td>
                                        <td class="col-md-2 text-center action-anchors">

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash js-delete-delivery-times"                                                 data-id="{{$delivery_time->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach

                            </div>

                            </tbody>
                        </table>
                            @else
                            <div class="alert alert-info" id="delivery-info"><strong>Information: </strong>Your company currently does not provide delivery!</div>
                            @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
