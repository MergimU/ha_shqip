@if($data)
    <form method="post" class="form-horizontal js-update-delivery-times" action="{{asset('settings/deliver-time/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('settings/deliver-time')}}"  class="form-horizontal
            js-delivery-times-create" method="post">
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="day">Choose day</label>
                    <div class="col-sm-5 ">
                        <select name="day" class="selectpicker form-control show-tick"
                                title="Choose day">
                            <option value="1">{{ucfirst('monday')}}</option>
                            <option value="2">{{ucfirst('tuesday')}}</option>
                            <option value="3">{{ucfirst('wednesday')}}</option>
                            <option value="4">{{ucfirst('thursday')}}</option>
                            <option value="5">{{ucfirst('friday')}}</option>
                            <option value="6">{{ucfirst('saturday')}}</option>
                            <option value="7">{{ucfirst('sunday')}}</option>

                        </select>
                        <div id="day-error" class="error font-re small error-js"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="start_at">Start at</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" name="start" class="form-control timepicker timepicker-24">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"></i></button>
                            </span>
                        </div>
                        <div id="start-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="start_at">End at</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" class="form-control timepicker timepicker-24" name="end">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-clock-o"></i></button>
                            </span>
                        </div>
                        <div id="end-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>



