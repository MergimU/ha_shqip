@include('admin.company-setting.setting-layout')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar font-green"></i>{{App\Models\Language::translateParagraph('holidays',
                    'Ferien')}} </div>

                    <div class="actions">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left"
                           title="Add new holiday"
                           class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                           holiday-create-js">
                        </a>
                    </div>

                <div class="portlet-body" >
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover "  id="table-deliver-schedule">
                            <thead>
                            <tr>

                                <th> Name </th>
                                <th> Date: </th>
                            </tr>
                            </thead>
                            <tbody id="show-companies">
                            <div class="row">
                                @foreach($holidays as $holiday)
                                    <tr>
                                        <td class="col-md-4"> {{ucfirst($holiday->name)}} </td>
                                        <td class="col-md-3"> {{$holiday->date}} </td>
                                        <td class="col-md-2 text-center action-anchors">

                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top"
                                               title="Edit" class="btn btn-sm  glyphicon glyphicon-edit
                                               js-edit-holiday" data-id="{{$holiday->id}}"></a>

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash js-delete-holiday"
                                               data-id="{{$holiday->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach

                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
