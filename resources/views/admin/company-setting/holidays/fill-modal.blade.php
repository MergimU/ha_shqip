@if($data)
    <form method="post" class="form-horizontal js-update-holiday" action="{{asset('settings/holiday/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('settings/holiday')}}"  class="form-horizontal js-create-holiday"
                  method="post">
                @endif
                {{csrf_field()}}

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="name" name="name"
                               @if($data) value="{{$data->name}}" @endif placeholder="holiday name" required/>
                        <div id="name-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="name">Date</label>
                    <div class="col-sm-5">
                        <input data-provide="datepicker" class="datepicker form-control"
                               name="date">
                        <div id="date-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>



