@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/holidays-style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.css')}}">
@endsection

@section('title',' | Holidays')
@section('content')

    @include('admin.company-setting.holidays.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/holidays-script.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
@endsection