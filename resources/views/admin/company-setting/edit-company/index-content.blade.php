@include('admin.company-setting.setting-layout')
<br>
<hr>
<div class="row">
    <div class="col-md-3">
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->

                <div class="profile-userpic">
                    @if($company->img_path != null)
                    <img src="{{asset('storage/company-images/'.$company->img_path)}}" class="img-responsive"
                         alt=""> </div>
                @else
                    <img src="{{asset('storage/company-images/no-image.jpg')}}" class="img-responsive"
                         alt="">
            </div>
            @endif
                <!-- END SIDEBAR USERPIC -->


                <!-- SIDEBAR MENU -->

                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <h4><strong><u>Current company info:</u></strong></h4>
                    <div class="">
                        <div class="profile-stat-text"> Company name: <span
                                    class="badge">{{$company->name}}</span> </div>
                        <div class="profile-stat-text"> Company Location: <span
                                    class="badge">{{$company->location}}</span>  </div>
                        <div class="profile-stat-text"> Company Zip: <span
                                    class="badge">{{$company->zip}}</span> </div>
                        <div class="profile-stat-text"> Mobile phone: <span
                                    class="badge">{{$company->mobile}}</span> </div>
                        <div class="profile-stat-text"> Email: <span
                                    class="badge">{{$company->email}}</span> </div>
                        <div class="profile-stat-text"> Website: <span
                                    class="badge">{{$company->website}}</span> </div>
                    </div>
                </div>
                <!-- END STAT -->
            </div>
            <!-- END PORTLET MAIN -->
        </div>

    </div>

    <div class="col-md-9">
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-green-madison bold uppercase">{{App\Models\Language::translateParagraph('edit_company','Bearbeiten Sie Ihr Unternehmen')}} </span>
                            </div>
                            <ul class="nav nav-tabs">

                                <li class="@if((session('tab') == 1) || (session('tab') == null)) active @endif">
                                    <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Company Info</a>
                                </li>

                                <li class="@if(session('tab') == 2) active @endif">
                                    <a href="#tab_1_2" data-toggle="tab" aria-expanded="true"> Change Logo</a>
                                </li>

                                <li class="@if(session('tab') == 3) active @endif">
                                    <a href="#tab_1_3" data-toggle="tab" aria-expanded="true">Owner Info</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane @if((session('tab') == 1)|| session('tab') == null) active @endif" id="tab_1_1">

                                    <form role="form" action="{{asset('settings/edit-company/'.$company->id  ) }}"method="post">
                                        {{csrf_field()}}
                                        {{method_field('PUT')}}



                                        <input id="searchInput" class="controls" type="text" placeholder="Enter a location">
                                        <div id="map"></div>

                                        <input id="lat" type="hidden" name="latitude">
                                        <input id="lon" type="hidden" name="longitude">
                                        <input id="address" type="hidden" name="address">
                                        <input id="place_id" type="hidden" name="place_id">
                                        <input id="zip_code" type="hidden" name="zip_code">

                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('location') ?
                                            'error-message'
                                             : ''}}"></div>
                                            <label class="control-label" for="location">Location</label>
                                            <input type="text" name="location" value="{{old('location')}}"
                                                   placeholder="location" id="location" class="form-control">
                                            @if ($errors->has('location'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('location') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="form-group">
                                            <div class="form-group{{ $errors->has('zip') ? 'error-message' : ''
                                            }}"></div>
                                            <label class="control-label" for="zip">Zip Code</label>
                                            <input type="text" name="zip" placeholder="zip code"
                                                   class="form-control" id="zip" value="{{ old('zip') }}">
                                            @if ($errors->has('zip'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('zip') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group {{ $errors->has('mobile') ? 'error-message' :
                                            '' }}"></div>
                                            <label class="control-label" for="mobile">Mobile Phone
                                                Number</label>
                                            <input type="text" name="mobile" placeholder="022-76xxxxx"
                                                   class="form-control" id="mobile" value="{{old('mobile') }}">
                                            @if ($errors->has('mobile'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('mobile') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-group{{ $errors->has('email') ? 'error-message' :
                                            ''}}"></div>
                                            <label class="control-label" for="email">Email</label>
                                            <input type="text" name="email" placeholder="email"
                                                   class="form-control" id="email" value="{{old('email')}}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-group{{ $errors->has('website') ? 'error-message'
                                            : '' }}"></div>
                                            <label class="control-label" for="website">Website</label>
                                            <input type="text" name="website" placeholder="http://www.mywebsite.com"
                                                   class="form-control" value="{{old('website')}}"
                                                   id="website">
                                            @if ($errors->has('website'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('website') }}</strong>
                                                </span>
                                                @else
                                            @endif
                                        </div>

                                        <div class="margiv-top-10">
                                            <input type="submit" class="btn green" name="company-info" value="
                                             Save Changes">
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->


                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane @if(session('tab') == 2) active @endif" id="tab_1_2">
                                    <form action="{{asset('settings/edit-company/'.$company->id  )}}"
                                          role="form" method="post" enctype="multipart/form-data" >
                                        {{csrf_field()}}
                                        {{method_field('PUT')}}
                                        <div class="form-group">
                                            <div class="form-group{{ $errors->has('image') ? 'error-message'
                                            : '' }}"></div>
                                            @if ($errors->has('image'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                                <div>
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select image </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="hidden" value="" name="..."><input type="file" name="image" accept="image/*"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="margin-top-10">
                                            <input type="submit" class="btn green" name="save-image" value="
                                            Save Changes">
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                {{--End of Avatar Tab--}}

                                {{--Begin of Owner info tab--}}
                                <div class="tab-pane @if(session('tab') == 3) active @endif" id="tab_1_3">
                                    <form role="form" action="{{asset('settings/edit-company/'.$company->id  ) }}"method="POST">
                                        {{csrf_field()}}
                                        {{method_field('PUT')}}
                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('owner_name') ?
            'error-message'
             : ''}}"></div>
                                            <label class="control-label" for="owner_name">Owner Name</label>
                                            <input type="text" name="owner_name" value="{{ old('owner_name') }}"
                                                   placeholder="owner name" id="owner_name" class="form-control">
                                            @if ($errors->has('owner_name'))
                                                <span class="help-block"> <strong>{{ $errors->first('owner_name') }}</strong> </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('owner_phone') ?'error-message' : ''}}"></div>
                                            <label class="control-label" for="owner_phone">Owner Phone</label>
                                            <input type="text" name="owner_phone" value="{{ old('owner_phone') }}"
                                                   placeholder="owner phone" id="owner_phone" class="form-control">
                                            @if ($errors->has('owner_phone'))
                                                <span class="help-block"><strong>{{ $errors->first('owner_phone') }}</strong></span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('owner_email') ?'error-message': ''}}"></div>
                                            <label class="control-label" for="owner_email">Owner Email</label>
                                            <input type="text" name="owner_email" value="{{ old('owner_email')}}"
                                                   placeholder="owner email" id="owner_email" class="form-control">
                                            @if ($errors->has('owner_email'))
                                                <span class="help-block"> <strong>{{ $errors->first('owner_email') }}</strong> </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('official_name') ?'error-message': ''}}"></div>
                                            <label class="control-label" for="official_name">Official Company Name</label>
                                            <input type="text" name="official_name" value="{{ old('official_name')}}"
                                                   placeholder="official name" id="official_name" class="form-control">
                                            @if ($errors->has('official_name'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('official_name') }}</strong> </span> @endif</div>

                                        <div class="form-group">
                                            <div class="form-group {{$errors->has('registration_number') ?
            'error-message'
             : ''}}"></div>
                                            <label class="control-label" for="registration_number">Registration Number</label>
                                            <input type="text" name="registration_number" value="{{ old('registration_number')}}"
                                                   placeholder="owner registration number" id="registration_number" class="form-control">
                                            @if ($errors->has('registration_number'))
                                                <span class="help-block">
                    <strong>{{ $errors->first('registration_number') }}</strong>
                </span>
                                            @endif
                                        </div>

                                        <div class="margiv-top-10">
                                            <input type="submit" class="btn green" name="owner-info" value="
             Save Changes">
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                {{--End of owner info tab--}}
                                <!-- END CHANGE AVATAR TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>