
@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/company.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/styles/google-autocomplete-style.css')}}">
    <link href="{{asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css")}}" rel="stylesheet" type="text/css" />

@endsection

@section('title',' | Edit-Company')



@section('content')



@include('admin.company-setting.edit-company.index-content')

@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/company-script.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/scripts/google-autocomplete-script.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdtUGSTYJFV-IKbYxsvvaqQrpQXqYrgPI&libraries=places&callback=initMap"
            async defer></script>
    <script src="{{asset("assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js")}}" type="text/javascript"></script>

@endsection


