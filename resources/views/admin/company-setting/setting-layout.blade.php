<div>
    <a href="{{asset('settings/edit-company/')}}" type="button" class="btn btn-circle green btn-md">
        <i class="fa fa-edit"></i>
        {{App\Models\Language::translateParagraph('edit_company','Bearbeiten Sie Ihr Unternehmen')}}
    </a>

    <a href="{{asset('settings/holiday')}}" type="button" class="btn btn-circle green btn-md">
        <i class="fa fa-calendar"></i>
        {{App\Models\Language::translateParagraph('holidays','Ferien')}}
    </a>

    <a href="{{asset('settings/working-hours')}}" type="button" class="btn btn-circle green btn-md">
        <i class="fa fa-clock-o"></i>
        {{App\Models\Language::translateParagraph('working_hours','Arbeitszeit')}}
    </a>

    <a href="{{asset('settings/deliver-time')}}" type="button" class="btn btn-circle green btn-md">
        <i class="fa fa-calendar"></i>
        {{App\Models\Language::translateParagraph('delivery_schedule','Lieferungsplan')}}
    </a>

    <a href="{{asset('settings/delivery-price-category')}}" type="button" class="btn btn-circle green
    btn-md">
        <i class="fa fa fa-money"></i>
        Delivery price category
    </a>

    <a href="{{asset('settings/deliver-detail')}}" type="button" class="btn btn-circle green btn-md">
        <i class="fa fa-car"></i>
        {{App\Models\Language::translateParagraph('delivery_details','Lieferdetails')}}
    </a>


</div>