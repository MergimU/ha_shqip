@include('admin.company-setting.setting-layout')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-money font-green"></i>Delivery price category </div>

                    <div class="actions">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left"
                           title="Add new schedule for working hours"
                           class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                       delivery-price-category-create-js">
                        </a>
                    </div>


                <div class="portlet-body" >
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover "  id="table-deliver-schedule">
                            <thead>
                            <tr>

                                <th> <strong>From</strong> (km) </th>
                                <th>  <strong>To</strong> (km) </th>
                                <th> Price/Km </th>

                            </tr>
                            </thead>
                            <tbody id="show-companies">
                            <div class="row">
                                @foreach($delivery_prices as $delivery_price)
                                    <tr>
                                        <td class="col-md-4"> {{$delivery_price->from}} </td>
                                        <td class="col-md-3"> {{$delivery_price->to}} </td>
                                        <td class="col-md-1"> {{$delivery_price->price}} </td>
                                        <td class="col-md-2 text-center action-anchors">

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash
                                               js-delete-delivery-price-category"
                                               data-id="{{$delivery_price->id}}"></a>
                                        </td>
                                    </tr>
                                @endforeach

                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
