@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/delivery-price-category-style.css')}}">
@endsection

@section('title',' | Delivery Price Category')
@section('content')

    @include('admin.company-setting.delivery-price-category.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/delivery-price-category-script.js')}}"></script>
@endsection