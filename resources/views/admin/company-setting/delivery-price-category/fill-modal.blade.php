@if($data)
    <form  method="post" class="form-horizontal js-update-delivery-price-category" action="{{asset('settings/delivery-price-category/'.$data->id)}}">
        {{method_field('PUT')}}
        @else
            <form action="{{asset('settings/delivery-price-category')}}" class="form-horizontal
            js-create-delivery-price-category"  method="post">
                @endif
                {{csrf_field()}}

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="from">From</label>
                    <div class="col-sm-5 ">
                        <select name="from" class="selectpicker form-control show-tick"
                                title="Select km from" id="from">

                            @for($i=0; $i<10;$i++)
                                <option  @if(in_array($i*10,$from)) disabled @endif
                                value="{{$i*10}}">{{$i*10}} Km</option>
                            @endfor


                        </select>
                        <div id="from-error" class="error font-re small error-js"></div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-4 control-label" for="to">To</label>
                    <div class="col-sm-5 ">
                        <select name="to" class="selectpicker form-control show-tick"
                                title="Select km to" id="to">

                            @for($i=1; $i<10;$i++)
                                <option @if(in_array($i*10,$to)) disabled @endif
                                value="{{$i*10}}">{{$i*10}} Km</option>
                            @endfor

                        </select>
                        <div id="to-error" class="error font-re small error-js"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" for="price">Price per km</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="price" name="price"
                               @if($data) value="{{$data->price}}" @endif placeholder="price" required/>
                        <div id="price-error" class="error font-re small error-js"></div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" name="superadmin-save" class="btn btn-primary btn-block
                        submit-js" value="Save">Save</button>
                    </div>
                </div>
            </form>



