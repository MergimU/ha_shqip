@include('admin.company-setting.setting-layout')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-clock-o font-green"></i>{{App\Models\Language::translateParagraph('working_hours','Arbeitszeit')}} </div>

                    <div class="actions">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new schedule for working hours"
                           class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                       working-hours-create-js">
                        </a>
                    </div>


                <div class="portlet-body" >
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover "  id="table-deliver-schedule">
                            <thead>
                            <tr>

                                <th> Day </th>
                                <th> Start At: </th>
                                <th> End At: </th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody id="show-companies">
                            <div class="row">
                                @foreach($working_hours as $working_hour)
                                    <tr>
                                        <td class="col-md-4"> {{ucfirst($working_hour->day)}} </td>
                                        <td class="col-md-3"> {{$working_hour->start_at}} </td>
                                        <td class="col-md-1"> {{$working_hour->end_at}} </td>
                                        <td class="col-md-2 text-center action-anchors">

                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash js-delete-working-hours"
                                               data-id="{{$working_hour->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach

                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
