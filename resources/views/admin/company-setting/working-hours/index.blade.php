@extends('structure.layout')
@section('page-styles')
    <link rel="stylesheet" href="{{asset('assets/admin/styles/working-hours-style.css')}}">
@endsection

@section('title',' | Working Hours')
@section('content')

    @include('admin.company-setting.working-hours.index-content')

@endsection
@section('page-scripts')
    <script type="text/javascript" src="{{asset('assets/admin/scripts/working-hours-script.js')}}"></script>
@endsection