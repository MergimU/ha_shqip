@include('admin.company-setting.setting-layout')

<br>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered" >
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-car font-green"></i> {{App\Models\Language::translateParagraph('delivery_details','Lieferdetails')}}</div>

                @if($delivery_details->isEmpty())

                    <div class="actions">
                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Add new delivery details" class="fa fa-plus font-black btn btn-circle btn-icon-only btn-default
                       delivery-details-create-js">
                        </a>

                    </div>

                    @endif

                <div class="portlet-body" >
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover "  >
                            <thead>
                            <tr>

                                <th> Free distance delivery </th>
                                <th> Minimum Orders </th>
                                <th> Max distance delivery </th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody id="show-companies">
                            <div class="row">
                                @foreach($delivery_details as $delivery_detail)
                                    <tr>
                                        <td class="col-md-4">
                                            {{$delivery_detail->free_distance_delivery}}</td>
                                        <td class="col-md-3"> {{$delivery_detail->min_order}} CHF</td>

                                        <td class="col-md-2"> {{$delivery_detail->max_distance_delivery}}  </td>

                                        <td class="col-md-2 text-center action-anchors">
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-sm
                                            glyphicon glyphicon-edit js-edit-delivery-details" data-id="{{$delivery_detail->id}}"></a>
                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                               data-placement="top" title="Delete" class="btn delete-anchor
                                               btn-sm glyphicon glyphicon-trash
                                               js-delete-delivery-details"
                                               data-id="{{$delivery_detail->id}}"></a>

                                        </td>
                                    </tr>
                                @endforeach
                            </div>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
