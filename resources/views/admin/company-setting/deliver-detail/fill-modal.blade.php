@if($data)
         <form method="post" class="form-horizontal js-update-delivery-detail" action="{{asset('settings/deliver-detail/'.$data->id)}}">
             {{method_field('PUT')}}
             @else
                 <form action="{{asset('settings/deliver-detail')}}" class="form-horizontal js-delivery-detail-create" method="post">
                     @endif
                {{csrf_field()}}

                 <div class="form-group">
                     <label class="col-sm-4 control-label" for="distance">Free distance delivery</label>
                     <div class="col-sm-5">
                         <input type="text" class="form-control" id="distance"
                                name="distance" @if($data) value="{{$data->free_distance_delivery}}" @endif placeholder="in kilometers"
                                required/>
                         <div id="free-distance-deliver-error" class="error font-re small error-js"></div>
                     </div>
                 </div>

                 <div class="form-group">
                     <label class="col-sm-4 control-label" for="order">Minimum order</label>
                     <div class="col-sm-5">
                         <input type="text" class="form-control" id="order" name="order" @if($data) value="{{$data->min_order}}" @endif
                         placeholder="in CHF"/>
                         <div id="min-order-error" class="error font-re small error-js"></div>
                     </div>
                 </div>

                     <div class="form-group">
                         <label class="col-sm-4 control-label" for="">Max distance delivery</label>
                         <div class="col-sm-5">
                             <input type="text" class="form-control" id="max"
                                    name="max" @if($data) value="{{$data->max_distance_delivery}}" @endif placeholder="in kilometers"
                                    required/>
                             <div id="max-distance-delivery-error" class="error font-re small
                             error-js"></div>
                         </div>
                     </div>

                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-block submit-js" name="save" value="Save">Save</button>
                    </div>
                </div>
            </form>



