@include('structure.head')

<head>
    <title>Your JavaScript is disabled!!!</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary" style="margin-top: 100px;">
                <div class="panel-heading">
                    JavaScript is Required!
                </div>
                <div class="panel-body">
                    <div class="alert alert-warning">
                        <h5 style="color: red;">   We are sorry, but this site does not work properly without JavaScript. Please go and enable JavaScript, then come back to use this site!</h5>
                        <p>If you already enabled JavaScript <a href="{{asset('admin')}}">Click to come back to the site</a></p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
</body>
