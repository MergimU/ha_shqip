/* Show / Hide Logo when navigation toggle is clicked */
$('.sidebar-toggler').on('click', function () {
    if ($('.logo').hasClass('show-logo')) {
        $('.logo').removeClass('show-logo');
    } else {
        $('.logo').addClass('show-logo');
    }
});


function loadModal(url, title, param) {
    var globalModal = $("#global-modal");

    $.ajax({
        url: url, data: param, success: function (response) {
            $(".modal-title").html(title);
            $(".modal-body").html(response);
            globalModal.modal({
                show: true

            });
            $('.selectpicker').selectpicker();
            $('.date').datepicker({autoclose: true, todayHighlight: true, format: 'yyyy-mm-dd'});
            $('input.timepicker').timepicker({
                showMeridian: false
            });
            $('.datepicker').datepicker({
               format: 'yyyy/mm/dd'
            });


            globalModal.on('hidden', function () {
                $(".modal-title").html("");
                $(".modal-body").html("");
            });
        }
    });
};


function ajaxStoreUpdate(url, data, submit_button, errorAction) {
    debugger;
    var ajaxLoadPath = window.location.pathname + location.search;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function () {
            debugger;
            var pageContent = $('.container');
            $.ajax({
                url: ajaxLoadPath, success: function (response) {
                    debugger;
                    pageContent.html(response);
                    //if exists
                    if ($('[data-toggle="tooltip"]').length > 0) {
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                }
            });
            $('#global-modal').modal('hide');
            swal("Success!", "Data has been saved.", "success");

        },
        error: function (data) {
            submit_button.html('Save');
            submit_button.attr("disabled", false);
            var errors = data.responseJSON;
            errorAction(errors);
        }
    });
}

function ajaxDelete(url) {
    debugger;
    var token = document.getElementsByName("_token")[0].value;
    var ajaxLoadPath = window.location.pathname + location.search;
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url,
            data: {_method: "DELETE", _token: token},

            success: function () {
                //reload the page
                swal("Deleted!", "Data has been deleted.", "success");
                var pageContent = $('.container');
                $.ajax({
                    url: ajaxLoadPath, success: function (response) {
                        pageContent.html(response);
                        //if not exist
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                });
            }
        });
    })
}


function alertMessage(time) {
    setTimeout(function () {
        $('.alert-success').fadeOut(2000);
    }, time);

}

//bootstrap selectpicker
$('.selectpicker').selectpicker();
//$('.date').datepicker();

//datepicker plugin
//var picker = new Pikaday({ field: $('#datepicker')[0] });

//bootstrap tooltip
$('[data-toggle="tooltip"]').tooltip();
