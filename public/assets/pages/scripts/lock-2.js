var Lock = function () {

    return {
        //main function to initiate the module
        init: function () {

             $.backstretch([
		        "../assets/partners/media/bg/1.jpg",
    		    "../assets/partners/media/bg/2.jpg",
    		    "../assets/partners/media/bg/3.jpg",
    		    "../assets/partners/media/bg/4.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		      });
        }

    };

}();

jQuery(document).ready(function() {
    Lock.init();
});