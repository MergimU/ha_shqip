$(document).on('click','.js-create-cuisine',function () {
    loadModal("cuisine/-1","Create new cuisine","");
});

$(document).on('click','.js-edit-cuisine',function () {
    id = $(this).data('id');
    loadModal("cuisine/"+id,"Create new cuisine","");
});

//update cuisine
$(document).on('submit','.js-update-cuisine',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-cuisine');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create cuisine
$(document).on('submit','.js-cuisine-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-cuisine-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete cuisine
$(document).on('click','.js-delete-cuisine',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});