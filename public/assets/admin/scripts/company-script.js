/**
 * Created by mergi on 5/25/2017.
 */
//load modal for edit purpose
$(document).on('click','.js-edit-company',function () {
    var id = $(this).data('id');
    loadModal("company/"+id,"Edit Company","");
});
//load modal for create company
$(document).on('click','.company-create-js',function () {
    loadModal("company/-1","Add Company","");
});


//update company
$(document).on('submit','.js-update-company',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-company');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');

        if(errors.zip)
            $('#zip-error').html('<strong>' + errors.zip + '</strong>');

        if(errors.location)
            $('#location-error').html('<strong>' + errors.location + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create company
$(document).on('submit','.js-create-company',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-create-company');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');

        if(errors.location)
            $('#location-error').html('<strong>' + errors.location + '</strong>');
        if(errors.zip)
            $('#zip-error').html('<strong>' + errors.zip + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete company
$(document).on('click','.js-delete-company',function () {
    debugger;
    var id =$(this).data('id');
     var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

//hide flash-message after 6 sec
/*(document).ready(function () {
    alertMessage(6000);
});

//disable button while form is submiting
$('#company').submit(function () {
    $('#company-save').attr('disabled',true);
});*/
