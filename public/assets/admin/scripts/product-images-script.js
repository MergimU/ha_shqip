Dropzone.options.productImage = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    addRemoveLinks:true,
    dictRemoveFile:'Remove Image',
    acceptedFile: 'images/*',
    success: function (file,response) {
       if(file.status == 'success'){
        handleDropzoneFileUpload.handleSuccess(response);
       }
       else{
        handleDropzoneFileUpload.handleError(response);
       }
    },
    init:function () {
        this.on("complete", function(file) {
            this.removeFile(file);
        });
    }

};

var handleDropzoneFileUpload = {
  handleError: function(response){
    console.log(response)
  },
    handleSuccess: function (response) {
        var imageList = $('#product-images ul');
        var baseUrl = window.location.origin;
        var imageSource = baseUrl + '/storage/product-images/' + response.img_path;

        //rendering complete product-images div with javascript
        $(imageList).append('<li><a href="'+imageSource+'" data-lightbox="product_image"><img ' +
            'src="'+imageSource+'" alt=""></a>' + '<div class="make-main-delete">' +
            '<a href="javascript:;void(0)" type="button" class="btn btn-circle dark btn-sm" data-id = "'+response.id+'" id="make-main">Make main</a>' +
            '<a href="javascript:;void(0)" data-id="'+response.id+'" class="remove-image"> <i ' +
            'class="fa ' + 'fa-trash"></i></a>' +'</div>'+
            '</li>')
    }
};



$(document).on('click','.remove-image',function () {
    debugger;
    var id = $(this).data('id');
    var url  = window.location.origin + '/product-image/' + id;
    var token = document.getElementsByName("_token")[0].value;

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        $.ajax({
            type: "POST",
            url: url,
            data: {_method: "DELETE", _token: token},

            success: function () {
                //reload the page
                swal("Deleted!", "Data has been deleted.", "success");
                var pageContent = $('#product-images');
                var ajaxLoadPath = window.location.pathname;
                $.ajax({
                    url: ajaxLoadPath, success: function (response) {
                        pageContent.html(response);
                        //if not exist
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                });
            }
        });
    })
});


$(document).on('click','#make-main',function () {
    debugger;
    var id = $(this).data('id');
    var url  = window.location.origin + '/product-image/' + id;
    var product_id = $('.product').val();
    var token = document.getElementsByName("_token")[0].value;
    $.ajax({
       method: 'POST',
        url: url,
        data:{
           _method:'PUT',
            _token:token,
            productId:product_id,
            imageId:id
        },
        success:function (response) {
            var pageContent = $('#product-images');
            var ajaxLoadPath = window.location.pathname;
            $.ajax({
                url: ajaxLoadPath, success: function (response) {
                    pageContent.html(response);
                    //if not exist
                }
            });
        }
    });
});