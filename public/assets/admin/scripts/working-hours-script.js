//load modal for working-hours
$(document).on('click','.js-edit-working-hours',function () {
    var id = $(this).data('id');
    loadModal("working-hours/"+id,"Edit working hours","");
});
//load modal for create working-hours
$(document).on('click','.working-hours-create-js',function () {
    loadModal("working-hours/-1","Add working hours ","");
});

//update availability
$(document).on('submit','.js-update-working-hours',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-update-working-hours');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    var splitStart = values.start.split(':'); // split it at the colons
    var splitEnd = values.end.split(':');
    var startAt =  (+splitStart[0]) * 60 + (+splitStart[1]); //get
    var endAt = (+splitEnd[0]) * 60 + (+splitEnd[1]);
    if(endAt < startAt){
        $('#start-error').html('<strong>' + "Start time should be less than End time!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        debugger;
        if(errors.day)
            $('#day-error').html('<strong>' + errors.day + '</strong>');
        if(errors.start)
            $('#start-error').html('<strong>' + errors.start + '</strong>');
        if(errors.end)
            $('#end-error').html('<strong>' + errors.end + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create availability
$(document).on('submit','.js-create-working-hours',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-create-working-hours');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    var splitStart = values.start.split(':'); // split it at the colons
    var splitEnd = values.end.split(':');
    var startAt =  (+splitStart[0]) * 60 + (+splitStart[1]); //get
    var endAt = (+splitEnd[0]) * 60 + (+splitEnd[1]);
    if(endAt < startAt){
        $('#start-error').html('<strong>' + "Start time should be less than End time!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        debugger;
        if(errors.day)
            $('#day-error').html('<strong>' + errors.day + '</strong>');
        if(errors.start)
            $('#start-error').html('<strong>' + errors.start + '</strong>');
        if(errors.end)
            $('#end-error').html('<strong>' + errors.end + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete company
$(document).on('click','.js-delete-working-hours',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});