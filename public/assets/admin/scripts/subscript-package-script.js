//load modal to create subscript package
$(document).on('click','.js-create-sub-package',function () {
    loadModal("subscription-package/-1","Create new subscript package","");
});
//load modal to create subscript package
$(document).on('click','.js-edit-sub-package',function () {
    id = $(this).data('id');
    loadModal("subscription-package/"+id,"Edit subscript package","");
});


//create  subscription package
$(document).on('submit','.js-sub-package-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-sub-package-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
        if(errors.category_id)
            $('#category-error').html('<strong>' + errors.category_id + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});



//update subscription package
$(document).on('submit','.js-update-sub-package',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-sub-package');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
        if(errors.category_id)
            $('#category-error').html('<strong>' + errors.category_id + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete category of subsricption
$(document).on('click','.js-delete-sub-package',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});