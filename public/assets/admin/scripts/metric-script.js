//load modal for create metric
$(document).on('click','.js-create-metric',function () {
    loadModal("metric/-1","Create new metric","");
});

//load modal for edit metric
$(document).on('click','.js-edit-metric',function () {
    id = $(this).data('id')
    loadModal("metric/"+id,"Edit metric","");
});

//update role
$(document).on('submit','.js-update-metric',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-metric');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//create role
$(document).on('submit','.js-metric-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-metric-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete metric
$(document).on('click','.js-delete-metric',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});
$(document).ready(function () {
    alertMessage(6000);
});