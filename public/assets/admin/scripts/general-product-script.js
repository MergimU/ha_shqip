/**
 * Created by mergi on 6/30/2017.
 */

//load modal for create product
$(document).on('click','.js-create-general-product',function () {
    loadModal("general-product/-1","Create new general product","");
});

//load modal for edit product
$(document).on('click','.js-edit-general-product',function () {
    id = $(this).data('id')
    loadModal("general-product/"+id,"Edit general product","");
});

//update product
$(document).on('submit','.js-update-general-product',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-general-product');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    values.ingredients = $('#ingredients').val();
    values.cuisine = $('#cuisine').val();
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
        if(errors.ingredients)
            $('#ingredients-error').html('<strong>' + errors.ingredients + '</strong>');
        if(errors.quantity)
            $('#quantity-error').html('<strong>' + errors.quantity + '</strong>');
        if(errors.prepare_time)
            $('#prepare_time-error').html('<strong>' + errors.prepare_time + '</strong>');
        if(errors.categories)
            $('#category-error').html('<strong>' + errors.categories + '</strong>');
        if(errors.metric)
            $('#metric-error').html('<strong>' + errors.metric + '</strong>');
        if(errors.cuisine)
            $('#cuisine-error').html('<strong>' + errors.cuisine + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//create product
$(document).on('submit','.js-general-product-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-general-product-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    values.ingredients = $('#ingredients').val();
    values.cuisine = $('#cuisine').val();
    /*values.recommendation = $("#recommendation ").val();*/
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
        if(errors.ingredients)
            $('#ingredients-error').html('<strong>' + errors.ingredients + '</strong>');
        if(errors.quantity)
            $('#quantity-error').html('<strong>' + errors.quantity + '</strong>');
        if(errors.prepare_time)
            $('#prepare_time-error').html('<strong>' + errors.prepare_time + '</strong>');
        if(errors.categories)
            $('#category-error-error').html('<strong>' + errors.categories + '</strong>');
        if(errors.metric)
            $('#metric-error').html('<strong>' + errors.metric + '</strong>');
        if(errors.cuisine)
            $('#cuisine-error').html('<strong>' + errors.cuisine + '</strong>');

    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete metric
$(document).on('click','.js-delete-general-product',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});