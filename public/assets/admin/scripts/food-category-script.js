$(document).on('click','.js-create-food-category',function () {
    loadModal("food-category/-1","Create new food category","");
});
$(document).on('click','.js-edit-food-category',function () {
    id = $(this).data('id');
    loadModal("food-category/"+id,"Edit food category","");
});


//update food category
$(document).on('submit','.js-update-food-category',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-food-category');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.cuisine_id)
            $('#cuisine-error').html('<strong>' + errors.cuisine_id + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//create food_category
$(document).on('submit','.js-food-category-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-food-category-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.cuisine_id)
            $('#cuisine-error').html('<strong>' + errors.cuisine_id + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete food-category
$(document).on('click','.js-delete-food-category',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});


$(document).ready(function () {
    alertMessage(6000);
});