/**
 * Created by mergi on 5/25/2017.
 */
//load modal for edit purpose
$(document).on('click','.js-edit-translation-group',function () {
    var id = $(this).data('id');
    loadModal("translation-group/"+id,"Edit translation group","");
});
//load modal for create company
$(document).on('click','.translation-group-create-js',function () {
    loadModal("translation-group/-1","Add translation group","");
});

//update translation group
$(document).on('submit','.js-update-translation-group',function (e) {
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-translation-group');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors)
    {
        if (errors.entity)
            $('#entity-error').html('<strong>' + errors.entity + '</strong>');

    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create translation group
$(document).on('submit','.js-create-translation-group',function (e) {
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-create-translation-group');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors) {
        if (errors.entity)
            $('#entity-error').html('<strong>' + errors.entity + '</strong>');

    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete company
$(document).on('click','.js-delete-translation-group',function () {
    var id =$(this).data('id');
     var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

//hide flash-message after 6 sec
/*(document).ready(function () {
    alertMessage(6000);
});

//disable button while form is submiting
$('#company').submit(function () {
    $('#company-save').attr('disabled',true);
});*/$
