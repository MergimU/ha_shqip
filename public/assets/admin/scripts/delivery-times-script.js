$(document).on('click','.delivery-times-create-js',function () {
    loadModal("deliver-time/-1","Create new delivery schedule","");
});

//edit modal
/*$(document).on('click','.js-edit-delivery-times',function () {
    var id = $(this).data('id');
    loadModal("company-setting/"+id,"Create new delivery schedule","");
});*/

//create delivery schedule
$(document).on('submit','.js-delivery-times-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-delivery-times-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    var splitStart = values.start.split(':'); // split it at the colons
    var splitEnd = values.end.split(':');
    var startAt =  (+splitStart[0]) * 60 + (+splitStart[1]); //get
    var endAt = (+splitEnd[0]) * 60 + (+splitEnd[1]);
    if(endAt < startAt){
        $('#start-error').html('<strong>' + "Start time should be less than End time!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }

    function errorAction(errors)
    {
        debugger;
        if(errors.day)
            $('#day-error').html('<strong>' + errors.day + '</strong>');

        if(errors.start)
            $('#start-error').html('<strong>' + errors.start + '</strong>');
        if(errors.end)
            $('#end-error').html('<strong>' + errors.end + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

$(document).on('change','#check-deliver',function () {
    debugger;
    var _checkValues = 0;

   if(document.getElementById('check-deliver').checked){
       var _checkValues = document.getElementById('check-deliver').value;
   }


    var baseUrl = window.location.origin;
    var company_id = $(this).data('company-id');
    var token = document.getElementsByName("_token")[0].value;
    var url = baseUrl+'/update-delivery/'+company_id
    $.ajax({
        url: url,
        method: "POST",
        data:{_method:"PUT",checkValues:_checkValues,companyId: company_id, _token:token},
        success:function (response) {
           showHideDelivery(_checkValues);
        }
    });
});

function showHideDelivery(checkValue) {
    if (checkValue == 1){
        $('#table-deliver-schedule').toggle();
        $('.actions').toggle();
    }
    else   {
        $('#table-deliver-schedule').toggle();
        $('.actions').toggle();

    }

}

/*$(document).ready(function () {
    var _checkValues = 0;
    if(document.getElementById('check-deliver').checked){
        var _checkValues = document.getElementById('check-deliver').value;
    }

    showHideDelivery(_checkValues);
});*/


//delete
$(document).on('click','.js-delete-delivery-times',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});