$(document).on('click','.js-create-partner',function () {
    loadModal("partner/-1","Create new partner","");
});

$(document).on('click','.js-edit-partner',function () {
    id = $(this).data('id');
    loadModal("partner/"+id,"Edit partner","");
});


//create partner
$(document).on('submit','.js-partner-create',function (e) {
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-partner-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    values.role_id = $("#select-roles ").val();
    if(values.password != "" &&  values.password !== values.confirm_password) {
        $('#password-error').html('<strong>' + "Passwords do not match!" + '</strong>');
        $('#confirm-password-error').html('<strong>' + "Passwords do not match!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.surname)
            $('#surname-error').html('<strong>' + errors.surname + '</strong>');
        if(errors.email)
            $('#email-error').html('<strong>' + errors.email + '</strong>');
        if(errors.password)
            $('#password-error').html('<strong>' + errors.password + '</strong>');
        if(errors.confirm_password)
            $('#confirm-password-error').html('<strong>' + errors.confirm_password + '</strong>');
        if(errors.company_id)
            $('#company-error').html('<strong>' + errors.company_id + '</strong>');
        if(errors.role_id)
            $('#role-error').html('<strong>' + errors.role_id + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//update partner
$(document).on('submit','.js-update-partner',function (e) {
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-update-partner');
    var url = form.attr('action');
    var selectedValues = [];
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
                values[field.name] = field.value;
    });
    values.role_id = $("#select-roles ").val();

    if(values.password != "" &&  values.password !== values.confirm_password) {
        $('#password-error').html('<strong>' + "Passwords do not match!" + '</strong>');
        $('#confirm-password-error').html('<strong>' + "Passwords do not match!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    console.log(values);
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.surname)
            $('#surname-error').html('<strong>' + errors.surname + '</strong>');
        if(errors.email)
            $('#email-error').html('<strong>' + errors.email + '</strong>');
        if(errors.password)
            $('#password-error').html('<strong>' + errors.password + '</strong>');
        if(errors.confirm_password)
            $('#confirm-password-error').html('<strong>' + errors.confirm_password + '</strong>');
        if(errors.company_id)
            $('#company-error').html('<strong>' + errors.company_id + '</strong>');
        if(errors.role_id)
            $('#role-error').html('<strong>' + errors.role_id + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete partner
$(document).on('click','.js-delete-partner',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

