$(document).on('click','.delivery-details-create-js',function () {
    loadModal("deliver-detail/-1","Create new delivery details","");
});

//edit modal
$(document).on('click','.js-edit-delivery-details',function () {
    var id = $(this).data('id');
    loadModal("deliver-detail/"+id,"Create new delivery details","");
});

//create delivery schedule
$(document).on('submit','.js-delivery-detail-create',function (e) {
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-delivery-detail-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.distance)
            $('#delivery-distance-error').html('<strong>' + errors.distance + '</strong>');

        if(errors.order)
            $('#min-order-error').html('<strong>' + errors.order + '</strong>');
        if(errors.max)
            $('#delivery-time-error').html('<strong>' + errors.max + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//update delivery details
$(document).on('submit','.js-update-delivery-detail',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-delivery-detail');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.distance)
            $('#delivery-distance-error').html('<strong>' + errors.distance + '</strong>');

        if(errors.order)
            $('#min-order-error').html('<strong>' + errors.order + '</strong>');
        if(errors.max)
            $('#delivery-time-error').html('<strong>' + errors.max + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete
$(document).on('click','.js-delete-delivery-details',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});