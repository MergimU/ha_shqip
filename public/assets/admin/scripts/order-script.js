var totalPrice = 0;
$(document).on('click','.btn-order',function () {
    var product_name = $(this).data('name');
    var product_price   = parseInt($(this).data('price'));
    var product_id   = $(this).data('id');


    $('.ordered-product').append('<div class="col-md-5"><span class="product-name">'+product_name+'</span></div>' +
        '<div class="col-md-5"><span class="product-price">'+product_price+' CHF</span> </div>' +
        '<div class="col-md-2"><span class="fa fa-remove product-remove"></span></div>');

    totalPrice = totalPrice + product_price;
    document.getElementById('order-total').innerText = "Total: "+totalPrice+ " CHF";
});

$(document).on('click','.product-remove',function () {

});
