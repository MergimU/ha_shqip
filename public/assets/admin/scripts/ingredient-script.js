$(document).on('click','.js-create-ingredient',function () {
    loadModal("general-product-ingredient/-1","Create new ingredient","");
});

$(document).on('click','.js-edit-ingredient',function () {
    id = $(this).data('id');
    loadModal("general-product-ingredient/"+id,"Create new ingredient","");
});

//update cuisine
$(document).on('submit','.js-update-ingredient',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-ingredient');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create cuisine
$(document).on('submit','.js-ingredient-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-ingredient-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete cuisine
$(document).on('click','.js-delete-ingredient',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});