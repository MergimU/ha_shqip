/**
 * Created by mergi on 6/30/2017.
 */
/**
 * Created by mergi on 5/25/2017.
 */
//load modal for edit availability
$(document).on('click','.js-edit-availability',function () {
    var id = $(this).data('id');
    loadModal("availability/"+id,"Edit product availability","");
});
//load modal for create availability
$(document).on('click','.availability-create-js',function () {
    loadModal("availability/-1","Add product availability ","");
});

//update availability
$(document).on('submit','.js-update-availability',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-update-availability');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    var splitStart = values.start.split(':'); // split it at the colons
    var splitEnd = values.end.split(':');
    var startAt =  (+splitStart[0]) * 60 + (+splitStart[1]); //get
    var endAt = (+splitEnd[0]) * 60 + (+splitEnd[1]);
    if(endAt < startAt){
        $('#start-error').html('<strong>' + "Start time should be less than End time!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        debugger;
        if(errors.product)
            $('#product-error').html('<strong>' + errors.product + '</strong>');

        if(errors.day)
            $('#day-error').html('<strong>' + errors.day + '</strong>');
        if(errors.start)
            $('#start-error').html('<strong>' + errors.start + '</strong>');
        if(errors.end)
            $('#end-error').html('<strong>' + errors.end + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create availability
$(document).on('submit','.js-create-availability',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-create-availability');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    var splitStart = values.start.split(':'); // split it at the colons
    var splitEnd = values.end.split(':');
    var startAt =  (+splitStart[0]) * 60 + (+splitStart[1]); //get
    var endAt = (+splitEnd[0]) * 60 + (+splitEnd[1]);
    if(endAt < startAt){
        $('#start-error').html('<strong>' + "Start time should be less than End time!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        debugger;
        if(errors.product)
            $('#product-error').html('<strong>' + errors.product + '</strong>');
        if(errors.day)
            $('#day-error').html('<strong>' + errors.day + '</strong>');
        if(errors.start)
            $('#start-error').html('<strong>' + errors.start + '</strong>');
        if(errors.end)
            $('#end-error').html('<strong>' + errors.end + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete company
$(document).on('click','.js-delete-availability',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

//hide flash-message after 6 sec
/*(document).ready(function () {
 alertMessage(6000);
 });

 //disable button while form is submiting
 $('#company').submit(function () {
 $('#company-save').attr('disabled',true);
 });*/$
