/**
 * Created by mergi on 6/30/2017.
 */
/**
 * Created by mergi on 5/25/2017.
 */

//load modal for create holidays
$(document).on('click','.holiday-create-js',function () {
    loadModal("holiday/-1","Add holiday ","");
});

//update holidays
$(document).on('submit','.js-update-holiday',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-update-holiday');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.date)
            $('#date-error').html('<strong>' + errors.date + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//create holidays
$(document).on('submit','.js-create-holiday',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-create-holiday');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.date)
            $('#date-error').html('<strong>' + errors.date + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete holidays
$(document).on('click','.js-delete-holiday',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

//hide flash-message after 6 sec
/*(document).ready(function () {
 alertMessage(6000);
 });

 //disable button while form is submiting
 $('#company').submit(function () {
 $('#company-save').attr('disabled',true);
 });*/$
