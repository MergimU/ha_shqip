/**
 * Created by mergi on 6/14/2017.
 */

//load modal for create subscription
$(document).on('click','.js-create-subscription',function () {
    loadModal("subscription/-1","Create new subscription","");
});

//load modal for edit subscription
$(document).on('click','.js-edit-subscription',function () {
    id = $(this).data('id')
    loadModal("subscription/"+id,"Edit subscription","");
});

//create subscription
$(document).on('submit','.js-subscription-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');

    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-subscription-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    var startDate = new Date(values.started);
    var endDate = new Date(values.ended);
    if (endDate < startDate){
        $('#started-error').html('<strong>' + "Started date should be less than Ended date!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        debugger;
        if(errors.company_id)
            $('#company-error').html('<strong>' + errors.company_id + '</strong>');
        if(errors.package_id)
            $('#package-error').html('<strong>' + errors.package_id + '</strong>');
        if(errors.category_id)
            $('#category-error').html('<strong>' + errors.category_id + '</strong>');
        if(errors.started)
            $('#started-error').html('<strong>' + errors.started + '</strong>');
        if(errors.ended)
            $('#ended-error').html('<strong>' + errors.ended + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//update subscription
$(document).on('submit','.js-update-subscription',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-subscription');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    var startDate = new Date(values.started);
    var endDate = new Date(values.ended);
    if (endDate < startDate){
        $('#started-error').html('<strong>' + "Started date should be less than Ended date!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }
    function errorAction(errors)
    {
        if(errors.started)
            $('#started-error').html('<strong>' + errors.started + '</strong>');
        if(errors.ended)
            $('#ended-error').html('<strong>' + errors.ended + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});


//delete partner
$(document).on('click','.js-delete-subscription',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});