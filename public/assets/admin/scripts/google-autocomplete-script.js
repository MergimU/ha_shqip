/**
 * Created by mergi on 8/29/2017.
 */
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



//initialize map
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.3769, lng: 8.5417},
        zoom: 13
    });

    //get search input for location
    var input = document.getElementById('searchInput');


    //option object for different properties such as restricion addresses, cities etc
    var options = {
        types: ['address'],
        componentRestrictions: {country: 'ch'}
    };
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);



    //initialize autocomplete function
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

//event when new location is typed
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);

        //get place attributes from getplace method
        var place = autocomplete.getPlace();
        var zip_code = document.getElementById('zip_code').value;
        for (var i = 0; i < place.address_components.length; i++) {
            for (var j = 0; j < place.address_components[i].types.length; j++) {
                if (place.address_components[i].types[j] == "postal_code") {
                    zip_code = place.address_components[i].long_name;
                }
            }
        }

        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }



        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);


        //console.log(place);
        //console.log(zip_code);


        document.getElementById('lat').value = place.geometry.location.lat();
        document.getElementById('lon').value = place.geometry.location.lng();;
        document.getElementById('address').value = place.formatted_address;
        document.getElementById('location').value = place.formatted_address;
        document.getElementById('place_id').value = place.place_id;
        document.getElementById('zip').value = zip_code;
    });
}