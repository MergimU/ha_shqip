/**
 * Created by mergi on 6/28/2017.
 */
//load modal for create recommendation
$(document).on('click','.js-create-recommendation',function () {
    loadModal("recommendation/-1","Create new recommendation","");
});

//load modal for edit recommendation
$(document).on('click','.js-edit-recommendation',function () {
    id = $(this).data('id')
    loadModal("recommendation/"+id,"Edit recommendation","");
});

//update role
$(document).on('submit','.js-update-recommendation',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-recommendation');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//create role
$(document).on('submit','.js-recommendation-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-recommendation-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete metric
$(document).on('click','.js-delete-recommendation',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});