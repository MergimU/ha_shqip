//load modal to create category of subscription
$(document).on('click','.js-create-sub-category',function () {
    loadModal("subscription-category/-1","Create new category of subscription","");
});
//load modal to create category of subscription
$(document).on('click','.js-edit-sub-category',function () {
    id = $(this).data('id');
    loadModal("subscription-category/"+id,"Edit category of subscription","");
});


//update category of subscription
$(document).on('submit','.js-update-sub-category',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-sub-category');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});



//create category of subscription
$(document).on('submit','.js-sub-category-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-sub-category-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});



//delete category of subsricption
$(document).on('click','.js-delete-sub-category',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});