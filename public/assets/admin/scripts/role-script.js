//load modal for create role
$(document).on('click','.js-create-role',function () {
    loadModal("role/-1","Create new role","");
});
//load modal for edit role
$(document).on('click','.js-edit-role',function () {
    id = $(this).data('id');
    loadModal("role/"+id,"Edit role","");
});

//update role
$(document).on('submit','.js-update-role',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-update-role');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.role_key)
            $('#role_key').html('<strong>' + errors.role_key + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//create role
$(document).on('submit','.js-role-create',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-role-create');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });
    function errorAction(errors)
    {
        debugger;
        if(errors.name)
            $('#name-error').html('<strong>' + errors.name + '</strong>');
        if(errors.role_key)
            $('#role_key').html('<strong>' + errors.role_key + '</strong>');
    }

    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete role
$(document).on('click','.js-delete-role',function () {
    debugger;
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

$(document).ready(function () {
    alertMessage(6000);
});