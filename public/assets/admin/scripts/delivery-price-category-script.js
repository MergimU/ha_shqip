$(document).on('click','.delivery-price-category-create-js',function () {
    loadModal("delivery-price-category/-1","Create new delivery price category","");
});

$(document).on('click','.js-edit-delivery-price-category',function () {
    id = $(this).data('id');
    loadModal("delivery-price-category/"+id,"Edit delivery price category","");
});


//create price category
$(document).on('submit','.js-create-delivery-price-category',function (e) {
    debugger;
    e.preventDefault();
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);

    var form = $('.js-create-delivery-price-category');
    var url = form.attr('action');
    var values = {};
    $.each(form.serializeArray(), function (i, field) {
        values[field.name] = field.value;
    });

    if(values.from > values.to){
        $('#from-error').html('<strong>' + "From range should be less than To range!" + '</strong>');
        submit_button.html("Save");
        submit_button.attr("disabled", false);
        return;
    }

    function errorAction(errors)
    {
        if(errors.from)
            $('#from-error').html('<strong>' + errors.from + '</strong>');
        if(errors.to)
            $('#to-error').html('<strong>' + errors.to + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//update partner
$(document).on('submit','.js-update-delivery-price-category',function (e) {
    e.preventDefault();
    debugger;
    var submit_button = $('.submit-js');
    submit_button.html("Saving");
    submit_button.attr("disabled", true);
    var form = $('.js-update-delivery-price-category');
    var url = form.attr('action');
    var selectedValues = [];
    var values = {};

    $.each(form.serializeArray(), function (i, field) {
                values[field.name] = field.value;
    });



    function errorAction(errors)
    {
        if(errors.from)
            $('#from-error').html('<strong>' + errors.from + '</strong>');
        if(errors.to)
            $('#to-error').html('<strong>' + errors.to + '</strong>');
        if(errors.price)
            $('#price-error').html('<strong>' + errors.price + '</strong>');
    }
    ajaxStoreUpdate(url,values,submit_button,errorAction);

});

//delete partner
$(document).on('click','.js-delete-delivery-price-category',function () {
    var id =$(this).data('id');
    var url = window.location.pathname +'/'+id;
    ajaxDelete(url);
});

