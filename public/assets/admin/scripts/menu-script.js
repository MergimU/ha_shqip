//make sortable elements between two divs
$( function() {
    $( "#sortable1, #sortable2" ).sortable({
        connectWith: ".connectedSortable"
    }).disableSelection();
} );

//save sorted products on array and pass this array to store method via ajax
$( "#sortable2" ).on( "sortreceive", function( event, ui ) {
    debugger;
    var lElements = document.getElementById("sortable2").children;
    var token = document.getElementsByName("_token")[0].value;
    var productsArray = [];
    for (i = 0; i < lElements.length; i++) {
        if (lElements[i].dataset.productId != undefined)
            productsArray.push(lElements[i].dataset.productId);
    }
    console.log(productsArray);

    $.ajax({
        type: "POST",
        url: "create-menu",
        data: { products_array: productsArray, _token: token},
        success: function () {
        }
    });
} );

//on remove event make ajax post resquest to delete element that was sorted
$( "#sortable2" ).on( "sortremove", function( event, ui ) {
    debugger;
    var productId = $(ui.item).data('id');
    var token = document.getElementsByName("_token")[0].value;

    $.ajax({
        type: "POST",
        url: "create-menu/"+productId,
        data: { _method:"DELETE", _token: token},
        success: function () {
        }
    });
} );

//function for search of general menu
$(document).on('keyup','.search-1',function () {
    var input = document.getElementById('search-1');
    var filter = input.value.toUpperCase();
    var ul = document.getElementsByClassName('products-1');
    var list = document.querySelectorAll('ul.products-1 li');

    for (var i = 0; i < list.length; i++) {
        var name = list[i].innerText;
        if (name.toUpperCase().indexOf(filter) > -1)
            list[i].style.display = 'inline-block';
        else
            list[i].style.display = 'none';
    }

});


//function for search of partner menu
$(document).on('keyup','.search-2',function () {
    var input = document.getElementById('search-2');
    var filter = input.value.toUpperCase();
    var ul = document.getElementsByClassName('products-2');
    var list = document.querySelectorAll('ul.products-2 li');

    for (var i = 0; i < list.length; i++) {
        var name = list[i].innerText;
        if (name.toUpperCase().indexOf(filter) > -1)
            list[i].style.display = 'inline-block';
        else
            list[i].style.display = 'none';
    }

});
