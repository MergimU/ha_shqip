<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodCategory extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function cuisine(){
       return $this->belongsTo('App\Models\Cuisine');
    }

    public function product(){
        return $this->hasMany('App\Models\Product');
    }

    public function general_product(){
        return $this->hasMany('App\Models\GeneralProduct');
    }
}
