<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ingredients extends Model
{
    use SoftDeletes;
    protected $dates = ['delete_at'];

    public function general_products()
    {
        return $this->hasMany('App\Models\GeneralProduct', 'ingredient_general_products','general_product_id', 'ingredient_id' );
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','ingredient_products','product_id','ingredient_id' );
    }

}
