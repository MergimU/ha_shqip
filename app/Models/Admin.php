<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Models\Role','admin_roles','admin_id','role_id');
    }

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }

    public function isRole($roleName)//if(Auth::user()->isRole('admin'));
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->role_key == $roleName)
            {
                return true;
            }
        }

        return false;
    }
}
