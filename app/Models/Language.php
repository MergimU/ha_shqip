<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function translations(){
        return $this->hasMany('App\Models\Translation');
    }

    static function translateParagraph($key, $paragraph)
    {
        $key = str_replace(' ', '_', strtolower($key));
        $defaultLanguageId = Language::where('name','german')->first()->id;

        $translation = Translation::where(function($query) use ($key, $defaultLanguageId)
        {
            $query->where('key',$key);
            $query->where('deleted_at',null);
            $query->where('language_id',session('language'));

            if($query->first() == null)
                $query->orWhere('key',$key)
                    ->where('language_id',$defaultLanguageId);
        })->first();

        if(!$translation)
        {
            $translation = new Translation();
            $translation->key = $key;
            $translation->language_id = $defaultLanguageId;
            $translation->translation = $paragraph;
            $translation->save();
        }

        return ucfirst($translation->translation);
    }
}
