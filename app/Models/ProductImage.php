<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use SoftDeletes;
    protected $fillable = ['product_id','img_path','main'];
    protected $dates = ['deleted_at'];
    protected $table = 'product_images';
    public function product(){
        return $this->belongsTo('App\Models\GeneralProduct');
    }
}
