<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslationGroups extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
