<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryDetailsModel extends Model
{
    protected $table = 'delivery_details';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
