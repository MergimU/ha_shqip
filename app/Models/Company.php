<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['img_path'];


    public function partner(){
        return $this->hasMany('App\Models\Partner');
    }

    public  function subscription(){
        return $this->hasOne('App\Models\Subscription');
    }

    public function deliverTimes(){
        return $this->hasMany('App\Models\DeliveryTimesModel');
    }

    public function products(){
        return $this->hasMany('App\Models\Product');
    }
}
