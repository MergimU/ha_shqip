<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralProduct extends Model
{
    protected $table = 'general_products';
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    public function ingredients()
    {
        return $this->belongsToMany('App\Models\Ingredients', 'ingredient_general_products','general_product_id', 'ingredient_id');
    }

    public function category(){
        return $this->belongsTo('App\Models\FoodCategory','food_category_id')->withTrashed();
    }

    public function metric(){
        return $this->belongsTo('App\Models\Metric');
    }

    public function cuisines()
    {
        return $this->belongsToMany('App\Models\Cuisine', 'cuisine_products','general_product_id', 'cuisine_id');
    }

    public function images(){
        return $this->hasMany('App\Models\ProductImage');
    }

}
