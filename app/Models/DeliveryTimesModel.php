<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryTimesModel extends Model
{
    protected  $table = 'delivery_times';

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }
}
