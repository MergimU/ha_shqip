<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuisine extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function foodCategory(){
        $this->hasMany('App\Models\FoodCategory');
    }
    public function general_products()
    {
        return $this->hasMany('App\Models\GeneralProduct', 'cuisine_products','general_product_id', 'ingredient_id' );
    }
}
