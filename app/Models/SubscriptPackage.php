<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptPackage extends Model
{
    protected $table = 'packages';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo('App\Models\SubscriptCategory','sub_cat_id');
    }
    public function subscrption(){
        return $this->hasMany('App\Models\Subscription');
    }
}
