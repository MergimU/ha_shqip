<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category(){
       return $this->belongsTo('App\Models\FoodCategory','food_category_id')->withTrashed();
    }

    public function company(){
       return $this->belongsTo('App\Models\Company');
    }
    public function availabilities(){
       return $this->hasMany('App\Models\Availability');
    }

    public function metric(){
       return $this->belongsTo('App\Models\Metric');
    }

    public function recommendations(){
        return $this->belongsToMany('App\Models\Recommendation','product_recommendations','product_id','recommendation_id');
    }

    public function ingredients(){
        return $this->belongsToMany('App\Models\Ingredients','ingredient_products','product_id','ingredient_id');
    }

    public function orders(){
        return $this->belongsToMany('App\Models\Order','order_details','product_id','order_id');
    }
}
