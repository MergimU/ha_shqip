<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function products(){
       return $this->hasMany('App\Models\Product','order_details','product_id','order_id');
    }
}
