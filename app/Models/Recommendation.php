<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recommendation extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function products(){
        return $this->hasMany('App\Models\Product','product_recommendations','product_id','recommendation_id');
    }
}
