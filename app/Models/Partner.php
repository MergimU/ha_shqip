<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    protected $table = 'admins';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }

    public function roles(){
        return $this->belongsToMany('App\Models\Role','admin_roles','admin_id','role_id');
    }
}
