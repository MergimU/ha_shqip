<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function partner(){
        return $this->hasMany('App\Models\Partner','admin_roles','admin_id','role_id');
    }
}
