<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptCategory extends Model
{
    protected $table = 'subscription_categories';
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function packages(){
        return $this->hasMany('App\Models\SubscriptPackages');
    }
}
