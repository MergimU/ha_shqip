<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Metric extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function products(){
       return $this->hasMany('App\Models\Product');
    }
    public function general_products(){
        return $this->hasMany('App\Models\GeneralProduct');
    }
}
