<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method()=="POST"){
            return [
                'company_id'=>'required',
                'package_id'=>'required',
                'started'=>'required',
                'ended'=>'required',
                'category_id'=>'required',
            ];
        }
        else{
            return [
                'company_id'=>'required',
                'package_id'=>'required',
                'started'=>'required',
                'ended'=>'required',
            ];
        }

    }

    public function messages()
    {
        return [
            'company_id.required'=>'The company field is required.',
            'package_id.required'=>'The package field is required.',
            'category_id.required'=>'The category field is required.',
        ];
    }
}
