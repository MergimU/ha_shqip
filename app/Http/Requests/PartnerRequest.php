<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method()=='POST'){
            return [
                'name'=>'required',
                'surname'=>'required',
                'email'=>'required|email|unique:admins,email',
                'password'=>'required|min:6',
                'confirm_password'=>'required|min:6',
                'company_id'=>'required',
                'role_id'=>'required',
            ];
        }
        else {
            return [
                'name'=>'required',
                'surname'=>'required',
                'email'=>'required|email',
                'password'=>'nullable|min:6',
                'confirm_password'=>'nullable|min:6',
                'company_id'=>'required',
                'role_id'=>'required',
            ];
        }
    }

    public  function messages()
    {

        return [
            'company_id.required'=>'You have to choose a company!',
            'role_id.required'=>'You have to choose a role!',
        ];

    }
}
