<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'ingredients'=>'required',
            'quantity'=>'required|numeric',
            'prepare_time'=>'required|numeric',
            'categories'=>'required',
            'metric'=>'required',
            'price'=>'required|numeric',
            'cuisine'=>'required',
        ];
    }
}
