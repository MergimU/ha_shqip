<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($_POST['company-info'])){
            session()->put('tab',1);
            return [
                'location'=>'required',
                'zip'=>'required|numeric|max:4',
                'mobile'=>'required|regex:/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}\d{4}$/',
                'email'=>'email',
                'website'=>'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
            ];
        }
        else if (isset($_POST['save-image'])){
            session()->put('tab',2);
            return[
              'image'=>'required|dimensions:min_width=150,min_height=150',
            ];
        }
        else if (isset($_POST['owner-info']))
        {
            session()->put('tab',3);
            return[
                'owner_name'=>'required',
                'owner_phone'=>'required|numeric',
                'owner_email'=>'required|email',
                'official_name'=>'required',
                'registration_number'=>'required|numeric'
            ];
        }

    }
    public  function  messages()
    {
        return [
            'mobile.regex' => 'Mobile phone number should be like this pattern: xxx-xxxxxxx',
            'website.regex'  => 'Website must given as url.',
            'image.dimensions' => 'Size of image should be greater than 150x150px.'
        ];
    }
}
