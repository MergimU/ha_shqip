<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Auth::guard('admin')->user()->isRole('superadmin')){
            return [
                'name'=>'required',
                'quantity'=>'required|numeric',
                'prepare_time'=>'required|numeric',
                'categories'=>'required',
                'metric'=>'required',
                'price'=>'required|numeric',
            ];
        }
        elseif (Auth::guard('admin')->user()->isRole('admin')){
            return[
                'prepare_time'=>'required|numeric',
                'categories'=>'required',
                'price'=>'required|numeric',
                'ingredients'=>'required',
            ];
        }

    }
}
