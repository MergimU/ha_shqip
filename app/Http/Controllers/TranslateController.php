<?php

namespace App\Http\Controllers;

use App\Http\Requests\TranslateRequest;
use App\Models\Language;
use App\Models\Translation;
use App\Services\TranslateService;
use Illuminate\Http\Request;

class TranslateController extends Controller
{
    protected $translateService;
    public function __construct(TranslateService $translateService)
    {
        $this->middleware('auth:admin');
        $this->translateService = $translateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $translations = Translation::paginate(20);
        if ($request->ajax())
            return view('admin.translations.translation.index-content',compact('translations'));
        return view('admin.translations.translation.index',compact('translations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TranslateRequest $request)
    {
        return $this->translateService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Translation::find($id);
        $languages = Language::all();
        return view('admin.translations.translation.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TranslateRequest $request, $id)
    {
        return $this->translateService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $translate = Translation::find($id);
        $translate->delete();
    }
}
