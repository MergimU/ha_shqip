<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryPriceCategoryRequest;
use App\Models\Company;
use App\Models\DeliveryDetailsModel;
use App\Models\DeliveryPriceCategory;
use App\Services\DeliveryPriceCategoryService;
use Illuminate\Http\Request;

class DeliveryPriceCategoryController extends Controller
{
    protected $deliveryPriceCategoryService;
    public function __construct(DeliveryPriceCategoryService $deliveryPriceCategoryService)
    {
        $this->middleware('auth:admin');
        $this->deliveryPriceCategoryService = $deliveryPriceCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $delivery_prices = DeliveryPriceCategory::all()->where('company_id', getCompanyId());
       /* $delivery_price = 0;
        $location  = 40;
        if ($this->deliveryPriceCategoryService->doesDeliver(40,55))
        {
            $delivery_price = $this->deliveryPriceCategoryService->deliveryPrice(33);
            dd($delivery_price);
        }
        else
        {
            dd('does not deliver');
        }*/
//        dd($delivery_price);
        if ($request->ajax())
            return view('admin.company-setting.delivery-price-category.index-content',compact
            ('delivery_prices'));
        return view('admin.company-setting.delivery-price-category.index',compact('delivery_prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryPriceCategoryRequest $request)
    {
        return $this->deliveryPriceCategoryService->store($request,getCompanyId());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DeliveryPriceCategory::find($id);
        $from = array();
        $to   = array();
        $from_tos = DeliveryPriceCategory::select('from','to')->where('company_id',getCompanyId())
            ->get();
        foreach ($from_tos as $from_to){
            array_push($from,$from_to->from);
            array_push($to,$from_to->to);
        }
        return view('admin.company-setting.delivery-price-category.fill-modal',compact('data', 'from','to'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->deliveryPriceCategoryService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery_price = DeliveryPriceCategory::find($id);
        $delivery_price->delete();
    }
}
