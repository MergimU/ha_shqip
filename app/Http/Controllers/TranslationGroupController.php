<?php

namespace App\Http\Controllers;

use App\Http\Requests\TranslationGroupRequest;
use App\Models\TranslationGroups;
use App\Services\TranslationGroupService;
use Illuminate\Http\Request;

class TranslationGroupController extends Controller
{
    protected $translationGroupService;
    public function __construct(TranslationGroupService $translationGroupService)
    {
        $this->middleware('auth:admin');
        $this->translationGroupService = $translationGroupService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $translation_groups = TranslationGroups::paginate(10);

        if ($request->ajax())
            return view('admin.translations.translation-group.index-content',compact('translation_groups'));
        return view('admin.translations.translation-group.index',compact('translation_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TranslationGroupRequest $request)
    {
        return $this->translationGroupService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TranslationGroups::find($id);
        return view('admin.translations.translation-group.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TranslationGroupRequest $request, $id)
    {
        return $this->translationGroupService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $translation = TranslationGroups::find($id);
        $translation->delete();
    }
}
