<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryTimesRequest;
use App\Models\WorkingHour;
use App\Services\WorkingHoursService;
use Illuminate\Http\Request;

class WorkingHoursController extends Controller
{

    protected $workingHoursService;
    public function __construct(WorkingHoursService $workingHoursService)
    {
        $this->middleware('auth:admin');
        $this->workingHoursService = $workingHoursService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $working_hours = WorkingHour::all()->where('company_id',getCompanyId());
        if ($request->ajax())
            return view('admin.company-setting.working-hours.index-content',compact('working_hours'));
        return view('admin.company-setting.working-hours.index',compact('working_hours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryTimesRequest $request)
    {
        return $this->workingHoursService->store($request,getCompanyId());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = WorkingHour::find($id);
        return view('admin.company-setting.working-hours.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryTimesRequest $request, $id)
    {

       return $this->workingHoursService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $working_hour = WorkingHour::find($id);
        $working_hour->delete();
    }
}
