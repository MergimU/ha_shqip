<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoodCategoryRequest;
use App\Models\Cuisine;
use App\Models\FoodCategory;
use App\Services\FoodCategoryService;
use Illuminate\Http\Request;

class FoodCategoryController extends Controller
{
    protected $foodCategoryService;
    public function __construct(FoodCategoryService $foodCategoryService)
    {
        $this->middleware('auth:admin');
        $this->foodCategoryService = $foodCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = FoodCategory::paginate(10);
        if ($request->ajax())
            return view('admin.food-category.index-content',compact('categories'));
        return view('admin.food-category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.food-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FoodCategoryRequest $request)
    {
        return $this->foodCategoryService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FoodCategory::find($id);
        $cuisines = Cuisine::all();
        return view('admin.food-category.fill-modal',compact('data','cuisines'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->foodCategoryService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = FoodCategory::find($id);
        $category->delete();
    }
}
