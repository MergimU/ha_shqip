<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryDetailsRequest;
use App\Models\DeliveryDetailsModel;
use App\Models\Partner;
use App\Services\DeliveryDetailsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeliveryDetailsController extends Controller
{
    protected $deliveryDetailService;
    public function __construct(DeliveryDetailsService $deliveryDetailsService)
    {
        $this->middleware('auth:admin');
        $this->deliveryDetailService = $deliveryDetailsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $delivery_details = DeliveryDetailsModel::all()->where('company_id',getCompanyId());
        if ($request->ajax())
            return view('admin.company-setting.deliver-detail.index-content',compact('delivery_details'));
        return view('admin.company-setting.deliver-detail.index',compact('delivery_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryDetailsRequest $request)
    {
        return $this->deliveryDetailService->store($request,getCompanyId());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DeliveryDetailsModel::find($id);
        return view('admin.company-setting.deliver-detail.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryDetailsRequest $request, $id)
    {
       return $this->deliveryDetailService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery = DeliveryDetailsModel::find($id);
        $delivery->delete();
    }
}
