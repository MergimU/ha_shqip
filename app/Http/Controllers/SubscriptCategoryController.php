<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriptCategoryRequest;
use App\Models\SubscriptCategory;
use App\Services\SubscriptCategoryService;
use Illuminate\Http\Request;

class SubscriptCategoryController extends Controller
{
    protected $categoryService;
    public function __construct(SubscriptCategoryService $categoryService)
    {
        $this->middleware('auth:admin');
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sub_categories = SubscriptCategory::paginate(10);
        if ($request->ajax())
            return view ('admin.subscript_category.index-content',compact('sub_categories'));
        return view ('admin.subscript_category.index',compact('sub_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptCategoryRequest $request)
    {
        return $this->categoryService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SubscriptCategory::find($id);
        return view('admin.subscript_category.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubscriptCategoryRequest $request, $id)
    {
      return  $this->categoryService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = SubscriptCategory::find ($id);
        $category->delete();
    }
}
