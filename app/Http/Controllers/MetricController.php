<?php

namespace App\Http\Controllers;

use App\Http\Requests\MetricRequest;
use App\Models\Metric;
use App\Services\MetricService;
use Illuminate\Http\Request;

class MetricController extends Controller
{
    protected $metricService;
    public function __construct(MetricService $metricService)
    {
        $this->middleware('auth:admin');
        $this->metricService = $metricService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metrics = Metric::paginate(10);
        if ($request->ajax())
            return view('admin.metrics.index-content',compact('metrics'));
        return view('admin.metrics.index',compact('metrics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.metrics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MetricRequest $request)
    {
        return $this->metricService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Metric::find($id);
        return view('admin.metrics.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->metricService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metric = Metric::find($id);
        $metric->delete();
    }
}
