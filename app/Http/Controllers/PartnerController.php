<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartnerRequest;
use App\Models\Company;
use App\Models\Partner;
use App\Models\Role;
use App\Services\PartnerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    protected $partnerService;
    public function __construct(PartnerService $partnerService)
    {
        $this->middleware('auth:admin');
        $this->partnerService = $partnerService;
    }

    public function index(Request $request)
    {
        $partners = Partner::paginate(10);
        if ($request->ajax())
            return view('admin.partners.index-content',compact('partners'));
        return view('admin.partners.index',compact('partners'));
    }

    public function create()
    {
        //
    }

    public function store(PartnerRequest $request)
    {
        return $this->partnerService->store($request);
    }

    public function show($id)
    {
        $data = Partner::find($id);
        $roles = Role::all();
        $companies = Company::all();
        return view('admin.partners.fill-modal',compact('data','roles','companies','selectedRoles'));
    }

    public function edit($id)
    {
        //
    }


    public function update(PartnerRequest $request, $id)
    {
        return $this->partnerService->update($request,$id);
    }

    public function destroy($id)
    {
        $partner = Partner::find($id);
        $partner->delete();
    }
}
