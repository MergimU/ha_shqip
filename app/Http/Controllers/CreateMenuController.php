<?php

namespace App\Http\Controllers;

use App\Models\GeneralProduct;
use App\Models\Partner;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreateMenuController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $existing_product_ids = Product::where('company_id',getCompanyId())->pluck('general_product_id');


        $existing_general_products = Product::whereIn('general_product_id',$existing_product_ids)->where('company_id',getCompanyId())->get();


        $non_existing_general_products = GeneralProduct::whereNotIn('id',$existing_product_ids)->get();

        return view('admin.create-menu.index', compact('existing_general_products',
            'non_existing_general_products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //gets array of general_product_ids from ajax request
        $product_ids = $request->products_array;
        foreach($product_ids as $product_id)
        {
           $general_product = GeneralProduct::find($product_id);

           //get all ingredients from general_menu and pass to the menu of company
           $ingredients = array();
            foreach ($general_product->ingredients as $ingredient){
                $ingredients[] = $ingredient->id;
            }

           //kontrollon a egziston produkti ndatabaz
            //nese po continue nese jo e shton ni instance
            $company_product = Product::where('name', $general_product->name)->where('company_id',
                getCompanyId())->first();
            if(!$company_product)
            {
                $new_product = new Product();
                $new_product->food_category_id   = $general_product->food_category_id;
                $new_product->metric_id          = $general_product->metric_id;
                $new_product->name               = $general_product->name;
                $new_product->quantity           = $general_product->quantity;
                $new_product->price              = $general_product->price;
                $new_product->prepare_time       = $general_product->prepare_time;
                $new_product->company_id         = getCompanyId();
                $new_product->general_product_id = $general_product->id;
                $new_product->save();
                $new_product->ingredients()->sync($ingredients,false);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
    }
}
