<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanySettingsRequest;
use App\Http\Requests\DeliveryTimesRequest;
use App\Models\Company;
use App\Models\DeliveryTimesModel;
use App\Models\Partner;
use App\Services\CompanySettingsService;
use App\Services\DeliveryTimeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeliveryTimesController extends Controller
{
    protected $deliveryTimeService;
    public function __construct(DeliveryTimeService $deliveryTimeService)
    {
        $this->middleware('auth:admin');
        $this->deliveryTimeService = $deliveryTimeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $partner_id = Partner::where('company_id',getCompanyId())->get();    //get partner related to its company
        $delivery_times = DeliveryTimesModel::all()->where('company_id',getCompanyId()); // get delivery times // related to company
        if ($request->ajax())
            return view('admin.company-setting.deliver-time.index-content',compact('delivery_times'));
        return view('admin.company-setting.deliver-time.index',compact('delivery_times'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryTimesRequest $request)
    {
                                       //get company related to partner
        return $this->deliveryTimeService->store($request,getCompanyId());
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DeliveryTimesModel::find($id);
        return view('admin.company-setting.deliver-time.fill-modal',compact('data'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery = DeliveryTimesModel::find($id);
        $delivery->delete();
    }
}
