<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralProductRequest;
use App\Models\Cuisine;
use App\Models\FoodCategory;
use App\Models\GeneralProduct;
use App\Models\Ingredients;
use App\Models\Metric;
use App\Models\Product;
use App\Models\ProductImage;
use App\Services\GeneralProductService;
use Illuminate\Http\Request;

class GeneralProductController extends Controller
{
    protected $general_productService;
    public function __construct(GeneralProductService $generalProductService)
    {
        $this->middleware('auth:admin');
        $this->general_productService = $generalProductService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $general_products = GeneralProduct::all();
        if($request->ajax())
            return view('admin.general-product.index-content',compact('general_products'));
        return view('admin.general-product.index',compact('general_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralProductRequest $request)
    {
        return $this->general_productService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GeneralProductModel  $generalProductModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = GeneralProduct::find($id);
        $categories = FoodCategory::all();
        $metrics = Metric::all();
        $ingredients = Ingredients::all();
        $cuisines = Cuisine::all();
        return view('admin.general-product.fill-modal',compact('data','categories','metrics','ingredients',
            'cuisines'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GeneralProductModel  $generalProductModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $product = GeneralProduct::find($id);

        //get the img_path of 'make main' image product
        $main_product = ProductImage::where('product_id',$product->id)->where('main',1)->pluck('img_path')->first();


        $product_images = ProductImage::all()->where('product_id',$product->id);
        if ($request->ajax())
            return view('admin.general-product.edit-with-photos-ajax',compact('product','product_images','main_product'));
        return view('admin.general-product.edit-with-photos',compact('product','product_images','main_product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GeneralProductModel  $generalProductModel
     * @return \Illuminate\Http\Response
     */
    public function update(GeneralProductRequest $request, $id)
    {
        return $this->general_productService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GeneralProductModel  $generalProductModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $general_product = GeneralProduct::find($id);
        $general_product->delete();
    }
}
