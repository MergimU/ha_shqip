<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriptPackageRequest;
use App\Models\SubscriptCategory;
use App\Models\SubscriptPackage;
use App\Services\SubscriptPackageService;
use Illuminate\Http\Request;

class SubscriptPackageController extends Controller
{

    protected $packageService;
    public function __construct(SubscriptPackageService $packageService)
    {
        $this->middleware('auth:admin');
        $this->packageService = $packageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $packages = SubscriptPackage::with('category')->paginate(10);
        if ($request->ajax())
            return view('admin.subscript-package.index-content',compact('packages'));
        return view('admin.subscript-package.index',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptPackageRequest $request)
    {
        return $this->packageService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SubscriptPackage::find($id);
        $categories = SubscriptCategory::all();
        return view('admin.subscript-package.fill-modal',compact('data','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubscriptPackageRequest $request, $id)
    {
        return $this->packageService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = SubscriptPackage::find($id);
        $package->delete();
    }
}
