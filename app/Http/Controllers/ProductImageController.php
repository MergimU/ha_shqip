<?php

namespace App\Http\Controllers;

use App\Models\GeneralProduct;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductImageController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //find product by hidden field in 'edit-with-photos' view
        $product = GeneralProduct::find($request->product);
        $image = new ProductImage();

        if ($request->hasFile('file')){
            $file = $request->file('file');
            $file_name =uniqid(). '.' . $request->file('file')->getClientOriginalExtension();

            if ($file->move('storage/product-images',$file_name)){
                Image::make('storage/product-images/'.$file_name)->resize(180,180)->save('storage/product-thumbnails/'.$file_name);
                $image->product_id = $product->id;
                $image->img_path = $file_name;
                $image->save();
            }
            return $image;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //get productId from ajax request
        $product_id = $request->productId;

        //set all values of non trashed 'main' rows to '0'
        $images = ProductImage::where('product_id','=',$product_id)->update(['main' => 0]);

        //set all values of trashed 'main' rows to '1'
        $trashedRows = ProductImage::onlyTrashed()->update(['main' => 0]);

        //when is click 'make main' it gets image id with ajax request and update it to '1'
        $image = ProductImage::find($request->imageId)->update(['main' => 1]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = ProductImage::find($id);
        if($image->delete()){
            File::delete('storage/product-images/'.$image->img_path);
            File::delete('storage/product-thumbnails/'.$image->img_path);
        }
    }
}
