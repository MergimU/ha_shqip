<?php

namespace App\Http\Controllers;

use App\Http\Requests\CuisineRequest;
use App\Models\Cuisine;
use App\Services\CuisineService;
use Illuminate\Http\Request;

class CuisineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $cuisineService;
    public function __construct(CuisineService $cuisineService)
    {
        $this->middleware('auth:admin');
        $this->cuisineService = $cuisineService;
    }

    public function index(Request $request)
    {
        $cuisines = Cuisine::paginate(10);
        if($request->ajax())
            return view('admin.cuisine.index-content',compact('cuisines'));
        return view('admin.cuisine.index',compact('cuisines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.cuisine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CuisineRequest $request)
    {
        return $this->cuisineService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Cuisine::find($id);
        return view('admin.cuisine.fill-modal',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->cuisineService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cuisine = Cuisine::find($id);
        $cuisine->delete();
    }
}
