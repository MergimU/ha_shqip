<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Company;
use App\Models\FoodCategory;
use App\Models\Ingredients;
use App\Models\Metric;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Recommendation;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $productService;
    public function __construct(ProductService $productService)
    {
        $this->middleware('auth:admin');
        $this->productService = $productService;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getCompanies(){
        $partners = Company::all();
        return view('admin.product.companies',compact('partners'));
    }
    public function index(Request $request)
    {

        if(Auth::guard('admin')->user()->isRole('superadmin'))
        {
            $products = Product::all()->where('company_id',$request->company_id);
        }
        else{
            $products = Product::all()->where('company_id',getCompanyId());
        }

        if ($request->ajax())
            return view('admin.product.index-content',compact('products'));
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        return $this->productService->store($request,getCompanyId());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::find($id);
        $categories = FoodCategory::all();
        $metrics = Metric::all();
        $ingredients = Ingredients::all();
        $recommendations = Recommendation::all()->where('company_id',getCompanyId());
        return view('admin.product.fill-modal',compact('data','categories','metrics','recommendations','ingredients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        return $this->productService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
    }
}
