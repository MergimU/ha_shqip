<?php

namespace App\Http\Controllers;

use App\Http\Requests\AvailabilityRequest;
use App\Models\Availability;
use App\Models\Partner;
use App\Models\Product;
use App\Services\AvailabilityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AvailabilityController extends Controller
{
    protected $availabilityService;
    public function __construct(AvailabilityService $availabilityService)
    {
        $this->middleware('auth:admin');
        $this->availabilityService = $availabilityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $products = DB::table('products')->select('id')->where('company_id','=',getCompanyId())->pluck('id');
        $availabilities = Availability::whereHas('product', function($q) use($products)
        {
            $q->whereIn('id', $products);
        })->get();

        if ($request->ajax())
            return view('admin.availabilities.index-content',compact('availabilities','products'));
        return view('admin.availabilities.index',compact('availabilities','products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AvailabilityRequest $request)
    {
        return $this->availabilityService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Availability::find($id);
        $products = Product::all()->where('company_id',getCompanyId());
        $availabilities = Availability::all();
        return view('admin.availabilities.fill-modal',compact('data','products','availabilities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AvailabilityRequest $request, $id)
    {
        return $this->availabilityService->update($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $availability = Availability::find($id);
        $availability->delete();
    }
}
