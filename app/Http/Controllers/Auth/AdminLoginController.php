<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminLoginController extends Controller
{

    protected $redirectTo = '/admin';

    public function showLoginForm(){
        return view('admin.admin-login');
    }
    public function login(Request $request){
        $this->validate($request,[
           'email'=>'required|email',
            'password'=>'required|min:5'
        ]);

        $credentials = Input::only('email', 'password');

        if (Auth::guard('admin')->attempt($credentials,$request->remember))
        {
            return redirect()->intended($this->redirectPath());
        }
        return redirect()->back()->withInput($request->only('email','remeber'))->withErrors(['errors'=>'Email or Password does not match']);
    }


    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
    }


    public function logout()
    {

        Auth::guard('admin')->logout();
        session()->forget('language');
        session()->forget('tab');
        return redirect('admin-login');

    }
}


