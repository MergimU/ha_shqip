<?php

namespace App\Providers;

use App\Models\Language;
use App\Models\Partner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


        //a variable passes to multiple views
        View::composer('*', function ($view) {
            if (Auth::guard('admin')->check()){
                $partner = Partner::find(Auth::guard('admin')->user()->id);  //find partner with specific id
                $company = $partner->company;
                $view->with('company', $company);
            }
        });

        View::composer('*', function ($view) {

                $languages = Language::all();  //find partner with specific id
                $view->with('languages', $languages);
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
