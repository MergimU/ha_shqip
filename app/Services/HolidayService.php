<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 8/21/2017
 * Time: 9:11 AM
 */

namespace App\Services;


use App\Models\Holiday;

class HolidayService
{
    public  function store($request,$company){
        $holiday = new Holiday();
        $holiday->name = $request->input('name');
        $holiday->date = $request->date;
        $holiday->company_id = $company;
        $holiday->save();
        return;
    }


    public function update($request, $id){
        $holiday = Holiday::find($id);
        $holiday->name = $request->name;
        $holiday->date = $request->date;
        $holiday->save();
        return;
    }
}