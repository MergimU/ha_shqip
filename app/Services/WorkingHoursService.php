<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 8/21/2017
 * Time: 2:37 PM
 */

namespace App\Services;


use App\Models\WorkingHour;

class WorkingHoursService
{
    public  function store ($request, $company){
        $working_hour = new WorkingHour();
        $working_hour->day = $request->input('day');
        $working_hour->start_at = $request->start;
        $working_hour->end_at = $request->end;
        $working_hour->company_id = $company;
        $working_hour->save();
        return;
    }

    public function update ($request, $id){
        $working_hour = WorkingHour::find($id);
        $working_hour->day = $request->day;
        $working_hour->start_at = $request->start;
        $working_hour->end_at = $request->end;
        $working_hour->save();
        return;
    }
}