<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/30/2017
 * Time: 8:59 AM
 */

namespace App\Services;


use App\Models\Recommendation;

class RecommendationService
{
    public function store($request,$company){
        $recommendation = new Recommendation();
        $recommendation->name = $request->name;
        $recommendation->price = $request->price;
        $recommendation->company_id = $company;
        $recommendation->save();
        return;
    }

    public function update($request, $id){
        $recommendation = Recommendation::find($id);
        $recommendation->name = $request->name;
        $recommendation->price = $request->price;
        $recommendation->save();
        dd($recommendation);
        return;
    }
}