<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 7/18/2017
 * Time: 9:24 AM
 */

namespace App\Services;


use App\Models\GeneralProduct;

class GeneralProductService
{
    public function store($request){
        $product = new GeneralProduct();
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->prepare_time = $request->prepare_time;
        $product->food_category_id = $request->categories;
        $product->metric_id = $request->metric;
        $product->save();
        $product->cuisines()->sync($request->cuisine,false);
        return;

    }

    public function update($request, $id){
        $product = GeneralProduct::find($id);
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->prepare_time = $request->prepare_time;
        $product->food_category_id = $request->categories;
        $product->metric_id = $request->metric;
        $product->save();
        $product->ingredients()->sync($request->ingredients,true);
        $product->cuisines()->sync($request->cuisine,true);
        return;
    }

}