<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/8/2017
 * Time: 10:02 AM
 */

namespace App\Services;


use App\Models\Cuisine;

class CuisineService
{
 public function store ($request){
     $cuisine = new Cuisine();
     $cuisine->name = $request->name;
     $cuisine->save();
     return;
 }

 public function update ($request, $id){
        $cuisine = Cuisine::find($id);
        $cuisine->name = $request->name;
        $cuisine->save();
        return;
    }
}