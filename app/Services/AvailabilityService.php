<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 7/4/2017
 * Time: 9:56 AM
 */

namespace App\Services;


use App\Models\Availability;

class AvailabilityService
{
    public function store($request){
        $availability = new Availability();
        $availability->product_id = $request->product;
        $availability->days = $request->day;
        $availability->start_at = $request->start;
        $availability->end_at   = $request->end;
        $availability->save();
        return;
    }
    public function update($request,$id){
        $availability = Availability::find($id);
        $availability->product_id = $request->product;
        $availability->days = $request->day;
        $availability->start_at = $request->start;
        $availability->end_at   = $request->end;
        $availability->save();
        return;
    }
}