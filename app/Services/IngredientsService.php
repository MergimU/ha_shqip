<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 7/17/2017
 * Time: 7:12 PM
 */

namespace App\Services;


use App\Models\Ingredients;

class IngredientsService
{
    public function store($request){
        $ingredients = new Ingredients();
        $ingredients->name = $request->name;
        $ingredients->save();
        return;
    }

    public function update($request, $id){
        $ingredient = Ingredients::find($id);
        $ingredient->name = $request->name;
        $ingredient->save();
        return;
    }
}