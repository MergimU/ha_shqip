<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/14/2017
 * Time: 10:20 AM
 */

namespace App\Services;


use App\Models\SubscriptPackage;

class SubscriptPackageService
{
    public function store($request){
        $package                =   new SubscriptPackage();
        $package->name          =   $request->name;
        $package->price         =   $request->price;
        $package->sub_cat_id    =   $request->category_id;
        $package->save();
        return;
    }


    public function update($request,$id){
        $package                =   SubscriptPackage::find($id);
        $package->name          =   $request->name;
        $package->price         =   $request->price;
        $package->sub_cat_id    =   $request->category_id;
        $package->save();
        return;
    }
}