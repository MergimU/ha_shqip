<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 8/22/2017
 * Time: 1:42 PM
 */

namespace App\Services;


use App\Models\Company;
use App\Models\DeliveryDetailsModel;
use App\Models\DeliveryPriceCategory;

class DeliveryPriceCategoryService
{
    public function store($request,$company){
        $delivery_price             = new DeliveryPriceCategory();
        $delivery_price->from       = $request->from;
        $delivery_price->to         = $request->to;
        $delivery_price->price      = $request->price;
        $delivery_price->company_id = $company;
        $delivery_price->save();
        return;
    }


    public function update($request,$id){
        $delivery_price         = DeliveryPriceCategory::find($id);
        $delivery_price->from   = $request->from;
        $delivery_price->to     = $request->to;
        $delivery_price->price  = $request->price;
        $delivery_price->save();
        return;
    }

    public function restaurantClientDistance (){
        return 40;
    }

    public function doesDeliver($min_order_price,$location){
        $does_deliver     = Company::select('with_deliver')->where('id', getCompanyId())->first();
        $order_details    = DeliveryDetailsModel::select('min_order','max_distance_delivery')->where('company_id',getCompanyId())->first();
        return (
            $does_deliver->with_deliver
            && $min_order_price >= $order_details->min_order
            && $location <= $order_details->max_distance_delivery
        );
    }

    public function deliveryPrice($client_location){
        $free_distance_delivery = DeliveryDetailsModel::where('company_id',getCompanyId())->first()->free_distance_delivery;
        if ($free_distance_delivery < $client_location){
            $price = DeliveryPriceCategory::select('price')
                ->where('from','<', $this->restaurantClientDistance())
                ->where('to', '>=', $this->restaurantClientDistance())
                ->first()->price;
            return $price * ($client_location - $free_distance_delivery);
        }
        return 0;


    }


}