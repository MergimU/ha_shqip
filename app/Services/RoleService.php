<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/7/2017
 * Time: 3:08 PM
 */

namespace App\Services;


use App\Models\Role;

class RoleService
{
 public function store($request){
     $role          =   new Role();
     $role->name    =   $request->name;
     $role->role_key=   $request->role_key;
     $role->save();
     return;
 }


 public function update($request, $id){
     $role          =  Role::find($id);
     $role->name    =  $request->name;
     $role->role_key=   $request->role_key;
     $role->save();
     return;
 }
}