<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/24/2017
 * Time: 4:30 PM
 */

namespace App\Services;


use App\Models\DeliveryDetailsModel;

class DeliveryDetailsService
{
    public function store($request,$company_id){
        $delivery = new DeliveryDetailsModel();
        $delivery->company_id = $company_id;
        $delivery->free_distance_delivery = $request->input('distance');
        $delivery->min_order = $request->input('order');
        $delivery->max_distance_delivery = $request->input('max');
        $delivery->save();
        return;
    }

    public function update($request,$id){
        $delivery =  DeliveryDetailsModel::find($id);
        $delivery->free_distance_delivery = $request->input('distance');
        $delivery->min_order = $request->input('order');
        $delivery->max_distance_delivery = $request->input('max');
        $delivery->save();
        return;
    }
}