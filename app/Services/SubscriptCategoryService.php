<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/13/2017
 * Time: 2:27 PM
 */

namespace App\Services;


use App\Models\SubscriptCategory;

class SubscriptCategoryService
{
    public function store($request){
        $category       = new SubscriptCategory();
        $category->name = $request->name;
        $category->save();
        return;
    }

    public function update($request,$id){
        $category       = SubscriptCategory::find($id);
        $category->name = $request->name;
        $category->save();
        return;
    }
}