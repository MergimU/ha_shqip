<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 8/8/2017
 * Time: 4:49 PM
 */

namespace App\Services;


use App\Models\Translation;

class TranslateService
{
    public function store($request){
        $translate = new Translation();
        $translate->language_id = $request->language;
        $translate->key = str_replace(' ','_',$request->key);
        $translate->translation = $request->translation;
        $translate->save();
        return;
    }

    public function update($request,$id){
        $translate = Translation::find($id);
        $translate->language_id = $request->language;
        $translate->key = $request->key;
        $translate->translation = $request->translation;
        $translate->save();
        return;
    }
}