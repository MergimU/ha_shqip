<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/13/2017
 * Time: 1:04 AM
 */

namespace App\Services;


use App\Models\Partner;

class PartnerService
{
public function store($request){
    $partner              =   new Partner();
    $partner->name        =   $request->name;
    $partner->surname     =   $request->surname;
    $partner->email       =   $request->email;
    $partner->password    =   bcrypt($request->password);
    $partner->company_id  =   $request->company_id;
    $partner->save();
    $partner->roles()->sync($request->role_id,false);
    return;
}

    public function update($request,$id){
        $partner              =   Partner::find($id);
        $partner->name        =   $request->name;
        $partner->surname     =   $request->surname;
        $partner->email       =   $request->email;
        if($request->password)
            $partner->password    =   bcrypt($request->password);
        $partner->company_id  =   $request->company_id;
        $partner->save();
        $partner->roles()->sync($request->role_id,true);
        return;
    }
}