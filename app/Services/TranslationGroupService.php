<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 8/8/2017
 * Time: 2:27 PM
 */

namespace App\Services;


use App\Models\TranslationGroups;

class TranslationGroupService
{
    public function store($request){
        $translate_group = new TranslationGroups();
        $translate_group->entity = $request->entity;
        $translate_group->save();
        return;
    }

    public function update($request,$id){
        $translate_group = TranslationGroups::find($id);
        $translate_group->entity = $request->entity;
        $translate_group->save();
        return;
    }
}