<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/30/2017
 * Time: 10:36 AM
 */

namespace App\Services;


use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class ProductService
{
public function store($request,$company){
    $product = new Product();
    $product->name = $request->name;
    $product->quantity = $request->quantity;
    $product->price = $request->price;
    $product->prepare_time = $request->prepare_time;
    $product->food_category_id = $request->categories;
    $product->metric_id = $request->metric;
    $product->company_id = $company;
    $product->save();
    $product->ingredients()->sync($request->ingredients,false);
    $product->recommendations()->sync($request->recommendation,false);
    return;

}

public function update($request, $id){
    $product = Product::find($id);

    //if user is superadmin, it can create product with all permissions
    if (Auth::guard('admin')->user()->isRole('superadmin')){
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->prepare_time = $request->prepare_time;
        $product->food_category_id = $request->categories;
        $product->metric_id = $request->metric;
    }

    //if user is admin of company, it can update product with limited permissions
    elseif (Auth::guard('admin')->user()->isRole('admin')){
        $product->prepare_time = $request->prepare_time;
        $product->food_category_id = $request->categories;
        $product->price = $request->price;
    }
    $product->save();

    $product->ingredients()->sync($request->ingredients,true);
    $product->recommendations()->sync($request->recommendation,true);
    return;
}

}