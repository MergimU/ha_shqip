<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/7/2017
 * Time: 3:08 PM
 */

namespace App\Services;


use App\Models\FoodCategory;

class FoodCategoryService
{
 public  function store($request){
     $foodCategory = new FoodCategory();
     $foodCategory->name = $request->name;
     $foodCategory->cuisine_id = $request->cuisine_id;
     $foodCategory->save();
     return;
 }

 public function update($request, $id){
     $foodCategory = FoodCategory::find($id);
     $foodCategory->name = $request->name;
     $foodCategory->cuisine_id = $request->cuisine_id;
     $foodCategory->update();
     return;
  }
}