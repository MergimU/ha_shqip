<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/24/2017
 * Time: 2:42 AM
 */

namespace App\Services;


use App\Models\DeliveryTimesModel;

class DeliveryTimeService
{
public function store($request,$company_id){
    $delivery = new DeliveryTimesModel();
    $delivery->company_id = $company_id;
    $delivery->day = $request->day;
    $delivery->start_at = $request->start;
    $delivery->end_at = $request->end;
    $delivery->save();
    return;
}
}