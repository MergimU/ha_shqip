<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/7/2017
 * Time: 3:08 PM
 */

namespace App\Services;


use App\Models\Metric;

class MetricService
{
 public function store($request){
     $metric = new Metric();
     $metric->name = $request->name;
     $metric->save();
     return;
 }

 public function update($request, $id){
     $metric = Metric::find($id);
     $metric->name = $request->name;
     $metric->save();
     return;
 }
}