<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/14/2017
 * Time: 2:04 PM
 */

namespace App\Services;


use App\Models\Subscription;
use App\Models\SubscriptPackage;

class SubscriptionService
{
public function store($request){
    $subscrition = new Subscription();
    $subscrition->company_id = $request->company_id;
    $subscrition->package_id = $request->package_id;
    $subscrition->started_at = $request->started;
    $subscrition->ended_at = $request->ended;
    $subscrition->status   = $this->checkSubscription($request->ended);
    $subscrition->save();
    return;
}

    public function checkSubscription($end_date){
        $now = date('Y/m/d');
        return ($now < date('Y/m/d', strtotime($end_date)) ? 1 : null);
    }

    public function update($request,$id){
        $subscrition = Subscription::find($id);
        $subscrition->company_id = $request->company_id;
        $subscrition->package_id = $request->package_id;
        $subscrition->started_at = $request->started;
        $subscrition->ended_at = $request->ended;
        $subscrition->status   = $this->checkSubscription($request->ended);
        $subscrition->save();
        return;
    }
}