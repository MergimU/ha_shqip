<?php
/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/7/2017
 * Time: 11:24 AM
 */

namespace App\Services;


use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class CompanyService
{
    public function store($request){
        $company = new Company();
        $company->name = $request->name;
        $company->save();
    }

    //update company function
    public function update( $request, $id){
        $company = Company::find($id);
        if (isset($_POST['company-info'])){
            $company->location = $request->address;
            $company->zip      = $request->zip;
            $company->mobile   = $request->mobile;
            $company->email    = $request->email;
            $company->website  = $request->website;
            $company->latitude = $request->latitude;
            $company->longitude = $request->longitude;
            $company->place_id  = $request->place_id;


        }
        if (isset($_POST['save-image'])){
            if ($request->hasFile('image')){
                $image = $request->file('image');
                $file_name =time(). '.' . $request->file('image')->getClientOriginalExtension();
                $path = 'storage/company-images';
                $image->move($path, $file_name);
                $company->img_path = $file_name;
                Image::make('storage/company-images/'.$company->img_path)->resize(150, 150)->save('storage/company-images/'.$company->img_path);
            }
        }
        if (isset($_POST['owner-info'])){
            $company->owner_name = $request->owner_name;
            $company->owner_phone = $request->owner_phone;
            $company->owner_email = $request->owner_email;
            $company->official_name = $request->official_name;
            $company->registration_number = $request->registration_number;
        }
        $company->save();
        return redirect('edit-company');
    }

    public  function update_delivery($request, $id){
        $company = Company::find($id);
        $company->with_deliver = $request;
        $company->save();
        return;
    }
    public function updateSuperadmin($request,$id){
        $company = Company::find($id);

        if (Auth::user()->isRole('superadmin')){
            $company->name     = $request->name;
            $company->location = $request->location;
            $company->zip      = $request->zip;
        }
        $company->save();
        return;
    }


}