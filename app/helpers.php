<?php

use App\Models\Partner;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: mergi
 * Date: 6/24/2017
 * Time: 5:09 PM
 */

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}


function getCompanyId(){
    $partner = Partner::find(Auth::guard('admin')->user()->id);
    $company_id = $partner->company->id;
    return $company_id;
}